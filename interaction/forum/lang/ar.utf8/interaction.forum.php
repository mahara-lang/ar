<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage interaction-forum
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

$string['addpostsuccess'] = 'تمت إضافة المنشور بنجاح';
$string['addtitle'] = 'إضافة منتدى';
$string['addtopic'] = 'إضافة موضوع';
$string['addtopicsuccess'] = 'تمت إضافة موضوع بنجاح';
$string['autosubscribeusers'] = 'هل ترغب بتسجيل المستخدمين آلياً؟';
$string['autosubscribeusersdescription'] = 'اختر ما إذا كنت ترغب بأن يتم تسجيل مستخدمي المجموعة فب هذا المنتدى آلياً';
$string['Body'] = 'Body';
$string['cantaddposttoforum'] = 'لا يسمح لك بالنشر في هذا المنتدى';
$string['cantaddposttotopic'] = 'لا يسمح لك بنشر هذا الموضوع';
$string['cantaddtopic'] = 'لا يسمح لك بإضافة مواضيع إلى هذا المنتدى';
$string['cantdeletepost'] = 'لا يسمح لك بحذف منشورات في هذا المنتدى';
$string['cantdeletethispost'] = 'لا يسمح لك بحذف هذا المنشور';
$string['cantdeletetopic'] = 'لا يسمح لك بحذف مواضيع في هذا المنتدى';
$string['canteditpost'] = 'لا يسمح لك بتعديل هذا المنشور';
$string['cantedittopic'] = 'لا يسمح لك بتعديل هذا الموضوع';
$string['cantfindforum'] = 'تعذر العثور على المنتدى ذو الرقم التعريفي %s';
$string['cantfindpost'] = 'تعذر العثور على المنشور ذو الرقم التعريفي %s';
$string['cantfindtopic'] = 'تعذر العثور الموضوع ذو الرقم التعريفي %s';
$string['cantviewforums'] = 'لا يسمح لك بمشاهدة المنتديات في هذه المجموعة';
$string['cantviewtopic'] = 'لا يسمح لك بمشاهدة المواضيع في هذا المنتدى';
$string['chooseanaction'] = 'اختر إجراءاً';
$string['clicksetsubject'] = 'انقر لاختيار موضوعاً';
$string['Closed'] = 'مغلق';
$string['Close'] = 'إغلاق';
$string['closeddescription'] = 'يستطيع المراقبين و مدراء المجموعة فقط الرد على المواضيع المغلقة';
$string['Count'] = 'Count';
$string['createtopicusersdescription'] = 'إذا اخترت "جميع أعضاء المجموعة"، يمكن لأي أحد إنشاء مواضيع جديدة و الرد عليها و الرد على مواضيع حالية. و إذا اخترت "المراقبين و مدراء المجموعة"، فالمراقبين و مدراء المجوعة هم فقط من يستطيعون بدء مواضيع جديدة، و لكن عندما تكون المواضيع موجودة، فإنه يمكن لجميع المستخدمين نشر ردود عليها.';
$string['currentmoderators'] = 'المراقبون الحاليون';
$string['defaultforumtitle'] = 'نقاش عام';
$string['defaultforumdescription'] = '%s منتدى النقاش العام';
$string['deleteforum'] = 'حذف المنتدى';
$string['deletepost'] = 'حذف المنشور';
$string['deletepostsuccess'] = 'تم حذف المنشور بنجاح';
$string['deletepostsure'] = 'هل أنت متاكد من أنك تريد فعل هذا؟ لأنه لايمكن الرجوع عنه.';
$string['deletetopic'] = 'حذف الموضوع';
$string['deletetopicvariable'] = 'حذف الموضوع \'%s\'';
$string['deletetopicsuccess'] = 'تم حذف الموضوع بنجاح';
$string['deletetopicsure'] = 'هل أنت متاكد من أنك تريد فعل هذا؟ لأنه لايمكن الرجوع عنه.';
$string['editpost'] = 'تعديل المنشور';
$string['editpostsuccess'] = 'تم تعديل المنشور بنجاح';
$string['editstothispost'] = 'تعديلات هذا المنشور:';
$string['edittitle'] = 'تعديل المنتدى';
$string['edittopic'] = 'تعديل الموضوع';
$string['edittopicsuccess'] = 'تم تعديل الموضوع بنجاح';
$string['forumname'] = 'اسم المنتدى';
$string['forumposthtmltemplate'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>%s by %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">Reply to this post online</a></p>
<p><a href=\"%s\">Unsubscribe from this %s</a></p>
</div>";
$string['forumposttemplate'] = "%s by %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
لمشاهدة المنشور و الرد عليه عبر الإنترنت، اتبع هذا الرابط:
%s

لإلغاء اشتراكك من هذا %s، قم بزيارة:
%s";
$string['forumsuccessfulsubscribe'] = 'تم تسجيل المنتدى بنجاح';
$string['forumsuccessfulunsubscribe'] = 'تم إلغاء اشتراك المنتدى بنجاح';
$string['gotoforums'] = 'اذهب إلى المنتديات';
$string['groupadmins'] = 'مدراء المجموعة';
$string['groupadminlist'] = 'مدراء المجموعة:';
$string['Key'] = 'Key';
$string['lastpost'] = 'أحدث منشور';
$string['latestforumposts'] = 'أحدث منشورات المنتدى';
$string['Moderators'] = 'المراقبون';
$string['moderatorsandgroupadminsonly'] = 'المراقبون و مدراء المجموعة فقط';
$string['moderatorslist'] = 'المراقبون:';
$string['moderatorsdescription'] = 'يمكن للمراقبون تعديل و حذف مواضيع و منشورات. و يمكنهم أيضاً فتح، إغلاق، وضع و إزالة مواضيع على أنها لاصقة';
$string['name'] = 'منتدى';
$string['nameplural'] = 'منتديات';
$string['newforum'] = 'منتدى جديد';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newpost'] = 'منشور جديد: ';
$string['newtopic'] = 'موضوع جديد';
$string['noforumpostsyet'] = 'لا يوجد منشورات في هذه المجموعة بعد';
$string['noforums'] = 'لا يوجد منتديات في هذه المجموعة';
$string['notopics'] = 'لا يوجد مواضيع في هذا المنتدى';
$string['Open'] = 'فتح';
$string['Order'] = 'ترتيب';
$string['orderdescription'] = 'اختر اين تريد أن يتم ترتيب المنتدى مقارنة مع المنتديات الأخرى';
$string['Post'] = 'منشور';
$string['postbyuserwasdeleted'] = 'تم حذف منشور من قبل %s';
$string['postdelay'] = 'تأخير المنشور';
$string['postdelaydescription'] = 'الحد الأدنى من الوقت (بالدقائق) الذي يجب أن يمر قبل أن يتم إرسال منشور جديد إلى المشتركين في المنتدى. يمكن لكاتب هذا المنشور إجراء تعديلات أثناء هذا الوقت.';
$string['postedin'] = '%s تم نشره بتاريخ %s';
$string['Poster'] = 'الناشر';
$string['postreply'] = 'رد على منشور';
$string['Posts'] = 'منشورات';
$string['allposts'] = 'جميع المنشورات';
$string['postsvariable'] = 'منشورات: %s';
$string['potentialmoderators'] = 'المراقبين الكامنين';
$string['re'] ='Re: %s';
$string['regulartopics'] = 'مواضيع اعتيادية';
$string['Reply'] = 'الرد';
$string['replyforumpostnotificationsubject'] = 'Re: %s: %s: %s';
$string['replyto'] = 'الرد على: ';
$string['Sticky'] = 'لاصق';
$string['stickydescription'] = 'المواضيع اللاصقة موجودة على راس كل صفحة';
$string['stickytopics'] = 'مواضيع لاصقة';
$string['Subscribe'] = 'اشتراك';
$string['Subscribed'] = 'مشترك';
$string['subscribetoforum'] = 'الاشتراك بالمنتدى';
$string['subscribetotopic'] = 'الاشتراك بالموضوع';
$string['Subject'] = 'الموضوع';
$string['Topic'] = 'موضوع';
$string['Topics'] = 'مواضيع';
$string['topiclower'] = 'موضوع';
$string['topicslower'] = 'مواضيع';
$string['topicclosedsuccess'] = 'تم إغلاق المواضيع بنجاح';
$string['topicisclosed'] = 'هذا الموضوع مغلق. يمكن فقط للمراقبين و مدراء المجموعة نشر ردود جديدة';
$string['topicopenedsuccess'] = 'تم فتح المواضيع بنجاح';
$string['topicstickysuccess'] = 'تم ضبط المواضيع على أنها لاصقة بنجاح';
$string['topicsubscribesuccess'] = 'تم إشراك المواضيع بنجاح';
$string['topicsuccessfulunsubscribe'] = 'تم إلغاء إشتراك الموضوع بنجاح';
$string['topicunstickysuccess'] = 'Topic unset as sticky successfully';
$string['topicunsubscribesuccess'] = 'تم إلغاء اشتراك المواضيع بنجاح';
$string['topicupdatefailed'] = 'تعذر تحديث المواضيع';
$string['typenewpost'] = 'منشور منتدى جديد';
$string['Unsticky'] = 'Unsticky';
$string['Unsubscribe'] = 'إلغاء الاشتراك';
$string['unsubscribefromforum'] = 'إلغاء الاشتراك من المنتدى';
$string['unsubscribefromtopic'] = 'إلغاء الاشتراك من الموضوع';
$string['updateselectedtopics'] = 'تحديث المواضيع المختارة';
$string['whocancreatetopics'] = 'من يمكنه أن ينشئ مواضيع';
$string['youcannotunsubscribeotherusers'] = 'لا يمكنك إلغاء اشتراك المستخدمين الآخرين';
$string['youarenotsubscribedtothisforum'] = 'أنت لست مشتركاً في هذا المنتدى';
$string['youarenotsubscribedtothistopic'] = 'أنت لست مشتركاً في هذا الموضوع';

$string['today'] = 'اليوم';
$string['yesterday'] = 'البارحة';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';

$string['indentmode'] = 'Forum Indent Mode';
$string['indentfullindent'] = 'توسيع بشكل كامل';
$string['indentmaxindent'] = 'توسيع إلى أقصى حد';
$string['indentflatindent'] = 'No indents';
$string['indentmodedescription'] = 'Specify how topics in this forum should be indented.';
$string['maxindent'] = 'Maximum Indent Level';
$string['maxindentdescription'] = 'Set the maximum indention level for a topic. This only applies if Indent mode has been set to Expand to max';

$string['closetopics'] = 'إغلاق المواضيع الجديدة';
$string['closetopicsdescription'] = 'إذا اختياره، سيتم إغلاق جميع المواضيع الجديدة في هذا المنتدى افتراضياً. فقط المراقبين و مدراء المجموعات هم من يستطيعون الرد على المواضيع المغلقة.';

