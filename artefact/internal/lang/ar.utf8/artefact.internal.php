﻿<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'الصفحة الشخصية';

$string['profile'] = 'الصفحة الشخصية';
$string['myfiles'] = 'ملفاتي';

$string['mandatory'] = 'إلزامي';
$string['public'] = 'عام';

$string['aboutdescription'] = 'أدخل اسمك الأول و الأخير الحقيقيان. إذا كنت تريد إظهار اسماً مختلفاً للأشخاص في النظام، ضع ذلك الاسم على أنه اسم العرض الخاص بك.';
$string['infoisprivate'] = 'ستكون هذه المعلومات خاصة حتى تقوم بوضعها في صفحة إلكترونية مشتركة مع الآخرين.';
$string['viewmyprofile'] = 'اعرض صفحتي الشخصية';

// profile categories
$string['aboutme'] = 'عني';
$string['contact'] = 'معلومات الاتصال';
$string['messaging'] = 'إرسال الرسائل';
$string['general'] = 'عام';

// profile fields
$string['firstname'] = 'الاسم الأول';
$string['lastname'] = 'الاسم الأخير';
$string['fullname'] = 'الاسم بالكامل';
$string['institution'] = 'المؤسسة';
$string['studentid'] = 'الرقم التعريفي للطالب';
$string['preferredname'] = 'اسم العرض';
$string['introduction'] = 'المقدمة';
$string['email'] = 'عنوان البريد الإلكتروني';
$string['maildisabled'] = 'البريد الإلكتروني مُعطَّل';
$string['officialwebsite'] = 'عنوان موقع الإنترنت الرسمي';
$string['personalwebsite'] = 'عنوان موقع الإنترنت الشخصي';
$string['blogaddress'] = 'عنوان سجل الويب';
$string['address'] = 'العنوان البريدي';
$string['town'] = 'البلدة';
$string['city'] = 'المدينة/المنطقة';
$string['country'] = 'البلد';
$string['homenumber'] = 'هاتف المنزل';
$string['businessnumber'] = 'هاتف العمل';
$string['mobilenumber'] = 'الهاتف المحمول';
$string['faxnumber'] = 'رقم الفاكس';
$string['icqnumber'] = 'رقم آي سيك يو ICQ';
$string['msnnumber'] = 'دردشة إم إس أن MSN';
$string['aimscreenname'] = 'اسم الشاشة للأيه آي إمAIM';
$string['yahoochat'] = 'دردشة ياهوYahoo';
$string['skypeusername'] = 'اسم المستخدم لبرنامج المحادثة Skype';
$string['jabberusername'] = 'اسم المستخدم لبروتوكول جابر Jabber';
$string['occupation'] = 'المهنة';
$string['industry'] = 'Industry';

// Field names for view user and search user display
$string['name'] = 'الاسم';
$string['principalemailaddress'] = ' عنوان البريد الإلكتروني الرئيسي';
$string['emailaddress'] = 'عنوان البريد الإكتروني البديل';

$string['saveprofile'] = 'حفظ الصفحة الشخصية';
$string['profilesaved'] = 'تم حفظ الصفحة الشخصية بنجاح';
$string['profilefailedsaved'] = 'تعذر حفظ الصفحة الشخصية';


$string['emailvalidation_subject'] = 'التحقق من صحة البريد الإلكتروني';
$string['emailvalidation_body'] = <<<EOF
مرحباً %s,

لقد قمت بإضافة عنوان البريد الإلكتروني %s إلى حساب المستخدم الخاص بك في مهارة. يرجى زيارة الرابط أدناه لتفعيل هذا العنوان.

%s

إذا كان هذا البريد الإلكتروني يخصك و لكنك لم تطلب إضافته إلى حساب مهارة الخاص بك، اتبع الرابط لإلغاء تفعيل البريد الإلكتروني.

%s
EOF;

$string['validationemailwillbesent'] = 'سيتم إرسال بريد إلكتروني للتأكد من الصحة عندما تقوم بحفظ صفحتك الشخصية';
$string['validationemailsent'] = 'لقد تم إرسال بريد إلكتروني للتأكد من الصحة';
$string['emailactivation'] = 'تفعيل البريد الإلكتروني';
$string['emailactivationsucceeded'] = 'تمت عملية تفعيل البريد الإلكتروني بنجاح';
$string['emailalreadyactivated'] = 'البريد الإلكتروني مفعل مسبقاً';
$string['emailactivationfailed'] = 'تعذر تفعيل البريد الإلكتروني';
$string['emailactivationdeclined'] = 'لقد تم إلغاء تفعيل البريد الإلكتروني بنجاح';
$string['verificationlinkexpired'] = 'رابط التاكد من الصحة منهي الصلاحية';
$string['invalidemailaddress'] = 'عنوان البريد الإلكتروني غير صحيح';
$string['unvalidatedemailalreadytaken'] = 'عنوان البريد الإلكتروني الذي تحاول التأكد من صحته مأخوذ مسبقاً';
$string['addbutton'] = 'إضافة';

$string['emailingfailed'] = 'تم حفظ الصفحة الشخصية، و لكنه لم يتم إرسال رسائل البريد الإلكتروني إلى: %s';

$string['loseyourchanges'] = 'هل تريد إلغاء التغييرات؟';

$string['editprofile']  = 'تعديل الصفحة الشخصية';
$string['editmyprofile']  = 'تعديل صفحتي الشخصية';
$string['Title'] = 'Title';

$string['Created'] = 'تم إنشاؤه';
$string['Description'] = 'وصف';
$string['Download'] = 'تنزيل';
$string['lastmodified'] = 'آخر تعديل';
$string['Owner'] = 'المالك';
$string['Preview'] = 'معاينة';
$string['Size'] = 'الحجم';
$string['Type'] = 'النوع';

$string['profileinformation'] = 'معلومات الصفحة الشخصية';
$string['profilepage'] = 'الصفحة الشخصية';
$string['viewprofilepage'] = 'عرض الصفحة الصفحة';
$string['viewallprofileinformation'] = 'عرض جميع معلومات الصفحة الشخصية';

?>
