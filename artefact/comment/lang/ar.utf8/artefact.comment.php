<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'تعليق';
$string['Comment'] = 'تعليق';
$string['Comments'] = 'تعليقات';
$string['comment'] = 'تعليق';
$string['comments'] = 'تعليقات';

$string['Allow'] = 'سماح';
$string['allowcomments'] = 'السماح بالتعليقات';
$string['approvalrequired'] = 'Comments are moderated, so if you choose to make this comment public, it will not be visible to others until it is approved by the owner.';
$string['attachfile'] = "إرفاق ملف";
$string['Attachments'] = "مرفقات";
$string['cantedithasreplies'] = 'يمكنك تعديل فقط التعليق الأحدث';
$string['canteditnotauthor'] = 'أنت لست كاتب هذا التعليق';
$string['cantedittooold'] = 'يمكنك تعديل فقط التعليقات التي أجريت قبل أقل من %d دقائق';
$string['commentmadepublic'] = "جعل التعليق عام";
$string['commentdeletedauthornotification'] = "تم حذف تعليقك على %s:\n%s";
$string['commentdeletednotificationsubject'] = 'تم حذف التعليق على %s';
$string['commentnotinview'] = 'التعليق %d غير موجود في الصفحة الإلكترونية %d';
$string['commentremoved'] = 'تم حذف التعليق';
$string['commentremovedbyauthor'] = 'قام الكاتب بحذف التعليق';
$string['commentremovedbyowner'] = 'قام المالك بحذف التعليق';
$string['commentremovedbyadmin'] = 'قام المدير بحذف التعليق';
$string['commentupdated'] = 'تم تحديث التعليق';
$string['editcomment'] = 'تعديل التعليق';
$string['editcommentdescription'] = 'يمكنك تحدبث التعليقات التي أجريت قبل أقل من %d دقائق و أنه لم يكن قد أضيف ردود جديدة عليها. بعد هذا الوقت لا يزال بإمكانك حذف تعليقات و إضافة تعليقات.';
$string['entriesimportedfromleapexport'] = 'تم جلب المدخلات من صادر LEAP، التي تعذر جلبها ن أي مكان آخر.';
$string['feedback'] = 'تعليق';
$string['feedbackattachdirname'] = 'ملفات تعليق';
$string['feedbackattachdirdesc'] = 'الملفات المرفقة بالتعليقات على ملفك الشخصي';
$string['feedbackattachmessage'] = 'لقد تم إضافة الملف (الملفات) المرفقة إلى مجلدك %s';
$string['feedbackonviewbyuser'] = 'تعليق على %s من قبل %s';
$string['feedbacksubmitted'] = 'تم إرسال التعليق';
$string['makepublic'] = 'اجعله عام';
$string['makepublicnotallowed'] = 'لا يسمح لك بجعل هذا التعليق عام';
$string['makepublicrequestsubject'] = 'طلب تغيير التعليق من خاص إلى عام';
$string['makepublicrequestbyauthormessage'] = '%s قد طلبوا بأن تجعل تعليقهم عام.';
$string['makepublicrequestbyownermessage'] = '%s قد طلب بأن تجعل تعليقك عام.';
$string['makepublicrequestsent'] = 'لقد تم إرسال رسالة إلى %s لطلب جعل التعليق عام.';
$string['messageempty'] = 'الرسالة فارغة. يسمح بالرسالة الفارغة فقط إذا قمت بإرفاق اللف.';
$string['Moderate'] = 'Moderate';
$string['moderatecomments'] = 'Moderate comments';
$string['moderatecommentsdescription'] = 'ستبقى التعليقات خاصة إلى أن تقوم أنت بالموافقة عليها.';
$string['newfeedbacknotificationsubject'] = 'تعليق جديد على %s';
$string['placefeedback'] = 'وضع تعليق';
$string['reallydeletethiscomment'] = 'هل أنت متأكد من أنك تريد حذف هذا التعليق؟';
$string['thiscommentisprivate'] = 'هذا التعليق خاص';
$string['typefeedback'] = 'تعليق';
$string['youhaverequestedpublic'] = 'لقد طلبت جعل هذا التعليق عام.';

$string['feedbacknotificationhtml'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>%s قام بالتعليق على %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">الرد على هذا التعليق على الإنترنت</a></p>
</div>";
$string['feedbacknotificationtext'] = "%s علِّق على %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
لمشاهدة التعليق و الرد عليه على الإنترنت، اتبع الرابط التالي:
%s";
$string['feedbackdeletedhtml'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>A comment on %s was removed</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">%s</a></p>
</div>";
$string['feedbackdeletedtext'] = "تم حذف تعليق على %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
لمشاهدة %s على الإنترنت، اتبع هذا الرابط:
%s";
?>
