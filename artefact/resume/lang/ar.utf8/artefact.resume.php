<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-resume
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'السيرة الوظيفية';

// Tabs
$string['introduction'] = 'المقدمة';
$string['educationandemployment'] = 'التعليم و العمل';
$string['achievements'] = 'الإنجازات';
$string['goals'] = 'الأهداف';
$string['skills'] = 'المهارات';
$string['interests'] = 'الاهتمامات';

$string['myresume'] = 'سيرتي الوظيفية';
$string['mygoals'] = 'أهدافي';
$string['myskills'] = 'مهاراتي';
$string['coverletter'] = 'خطاب تقديم';
$string['interest'] = 'الاهتمامات';
$string['contactinformation'] = 'معلومات الاتصال';
$string['personalinformation'] = 'معلومات شخصية';
$string['dateofbirth'] = 'تاريخ الميلاد';
$string['placeofbirth'] = 'مكان الميلاد';
$string['citizenship'] = 'الجنسية';
$string['visastatus'] = 'حالة التأشيرة';
$string['female'] = 'أنثى';
$string['male'] = 'ذكر';
$string['gender'] = 'الجنس';
$string['maritalstatus'] = 'الحالة الاجتماعية';
$string['resumesaved'] = 'تم حفظ السيرة الوظيفية';
$string['resumesavefailed'] = 'تعذر تحديث سيرتك الوظيفية';
$string['educationhistory'] = 'التاريخ التعليمي';
$string['employmenthistory'] = 'التاريخ الوظيفي';
$string['certification'] = 'الشهادات، المؤهلات، و الجوائز';
$string['book'] = 'الكتب و المنشورات';
$string['membership'] = 'العضوية المهنية';
$string['startdate'] = 'تاريخ البدء';
$string['enddate'] = 'تاريخ الانتهاء';
$string['date'] = 'التاريخ';
$string['position'] = 'المنصب الوظيفي';
$string['qualification'] = 'المؤهل';
$string['title'] = 'اللقب';
$string['description'] = 'الوصف';
$string['employer'] = 'صاحب العمل';
$string['jobtitle'] = 'المسمى الوظيفي';
$string['jobdescription'] = 'وصف المنصب الوظيفي';
$string['institution'] = 'المؤسسة';
$string['qualtype'] = 'نوع المؤهل';
$string['qualname'] = 'اسم المؤهل';
$string['qualdescription'] = 'وصف المؤهل';
$string['contribution'] = 'مشاركة';
$string['detailsofyourcontribution'] = 'تفاصيل مشاركتك';
$string['compositedeleteconfirm'] = 'هل أنت متأكد من أنك تريد حذف هذا؟';
$string['compositesaved'] = 'تم الحفظ بنجاح';
$string['compositesavefailed'] = 'تعذر الحفظ';
$string['compositedeleted'] = 'تم الحذف بنجاح';
$string['backtoresume'] = 'العودة إلى سيرتي الوظيفية';
$string['personalgoal'] = 'الأهداف الشخصية';
$string['academicgoal'] = 'الأهداف الأكاديمية';
$string['careergoal'] = 'الأهداف الوظيفية';
$string['defaultpersonalgoal'] = '';
$string['defaultacademicgoal'] = '';
$string['defaultcareergoal'] = '';
$string['personalskill'] = 'المهارات الشخصية';
$string['academicskill'] = 'المهارات الأكاديمية';
$string['workskill'] = 'المهارات الوظيفية';
$string['goalandskillsaved'] = 'تم الحفظ بنجاح';
$string['resume'] = 'السيرة الوظيفية';
$string['current'] = 'الحالي';
$string['moveup'] = 'الانتقال إلى الأعلى';
$string['movedown'] = 'الانتقال إلى الأسفل';
$string['viewyourresume'] = 'اعرض سيرتك الوظيفية';
$string['resumeofuser'] = 'السيرة الوظيفية لـ %s';
$string['employeraddress'] = 'عنوان صاحب العمل';
$string['institutionaddress'] = 'عنوان المؤسسة';

?>
