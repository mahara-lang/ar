<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'ملفات';

$string['sitefilesloaded'] = 'تم تحميل ملفات الموقع';
$string['addafile'] = 'إضافة ملف';
$string['archive'] = 'أرشيف';
$string['bytes'] = 'bytes';
$string['cannoteditfolder'] = 'لا يوجد لديك إذن بإضافة متحتوى إلى هذا المجلد';
$string['cannoteditfoldersubmitted'] = 'لا يمكنك إضافة محتوى إلى مجلد في صفحة إلكترونية مرسلة.';
$string['cannotremovefromsubmittedfolder'] = 'لا يمكنك حذف محتوى إلى مجلد في صفحة إلكترونية مرسلة.';
$string['changessaved'] = 'تم حفظ التغييرات';
$string['clickanddragtomovefile'] = 'انقر و اسحب لنقل %s';
$string['contents'] = 'محتويات';
$string['copyrightnotice'] = 'إشعار بحقوق الطبع';
$string['create'] = 'إنشاء';
$string['Created'] = 'تم إنشاؤه';
$string['createfolder'] = 'إنشاء مجلد';
$string['confirmdeletefile'] = 'هل أنت متاكد من أنك تريد حذف هذا الملف؟';
$string['confirmdeletefolder'] = 'هل أنت متاكد من انك تريد حذف هذا المجلد؟';
$string['confirmdeletefolderandcontents'] = 'هل أنت متأكد من أنك تريد حذف هذا المجلد و جميع محتوياته؟';
$string['customagreement'] = 'اتفاقية مخصصة';
$string['Date'] = 'التاريخ';
$string['defaultagreement'] = 'اتفاقية افتراضية';
$string['defaultquota'] = 'الكوتا الإفتراضية';
$string['defaultquotadescription'] = 'يمكنك ضبط مقدار مساحة القرص التي سيستخدمها المستخدمون الجدد كوتا لهم هنا. لن تتغير كوتا المستخدمين الموجودين.';
$string['maxquotaenabled'] = 'Enforce a site-wide maximum quota';
$string['maxquota'] = 'الحد الأعلى من الكوتا';
$string['maxquotatoolow'] = 'لا يمكن أن يكون الحد العلى من الكوتا أقل من الكوتا الإفتراضية.';
$string['maxquotaexceeded'] = 'لقد قمت بتحديد كوتا أكبر من إعداد الحد الأعلى المتوفر لهذا الموقع (%s). حاول أن تحدد قيمة أقل أة اتصل بمدير الموقع لتطلب منه زيادة الحد الأعلى من الكوتا.';
$string['maxquotaexceededform'] = 'يرجى تحديد كوتا ملف أقل من %s.';
$string['maxquotadescription'] = 'يمكنك ضبط الحد الأعلى من الكوتا التي يمكن أن يمنحها مدير لمستخدم. لن تتأثر كوتا المستخدمين الموجودين.';
$string['deletingfailed'] =  'تعذر الحذف: الملف أو المجلد لم يعد موجوداً';
$string['deletefile?'] = 'هل أنت متأكد من أنك تريد حذف هذا الملف؟';
$string['deletefolder?'] = 'هل أنت متأكد من أنك تريد حذف هذا المجلد؟';
$string['Description'] = 'وصف';
$string['destination'] = 'وصف';
$string['Details'] = 'تفاصيل';
$string['Download'] = 'تنزيل';
$string['downloadfile'] = 'تنزيل %s';
$string['downloadoriginalversion'] = 'تنزيل النسخة الأصلية';
$string['editfile'] = 'تعديل ملف';
$string['editfolder'] = 'تعديل مجلد';
$string['editingfailed'] = 'تعذر تعديل: الملف أو المجلد لم يعد موجوداً';
$string['emptyfolder'] = 'مجلد فارغ';
$string['file'] = 'ملف'; // Capitalised to be consistent with names of all the other artefact types
$string['File'] = 'ملف';
$string['fileadded'] = 'تم اختيار الملف';
$string['filealreadyindestination'] = 'الملف الذي نقلته موجود مسبقاً في ذلك المجلد';
$string['fileappearsinviews'] = 'يظهر هذا الملف في واحدة أو أكثر من صفحاتك الإلكترونية.';
$string['fileattached'] = 'هذا الملف مرفق إلى %s عنصر (عناصر) أخرى  في صفحتك الإلكترونية.';
$string['fileremoved'] = 'تم حذف الملف';
$string['files'] = 'ملفات';
$string['Files'] = 'ملفات';
$string['fileexists'] = 'الملف موجود';
$string['fileexistsoverwritecancel'] =  'ملف بهذا الاسم موجود مسبقاً. يمكنك محاولة اسماً مختلفاً، أو أن تنسخ فوق ملف الموجود.';
$string['filelistloaded'] = 'تم تحميل لائحة الملف';
$string['filemoved'] = 'تم نقل الملف بنجاح';
$string['filenamefieldisrequired'] = 'حقل الملف إلزامي';
$string['fileinstructions'] = 'حمِّل صورك أو وثائقك أو ملفات أخرى لتضمينها في الصفحات الإلكترونية. قم بسحب و إدراج الأيقونات لنقل الملفات بين المجلدات.';
$string['filethingdeleted'] = '%s محذوف';
$string['filewithnameexists'] = 'ملف أو مجلد بالاسم "%s" موجود مسبقاً.';
$string['folder'] = 'مجلد';
$string['Folder'] = 'مجلد';
$string['folderappearsinviews'] = 'يظهر هذا المجلد في واحدة أو أكثر من صفحاتك الإلكترونية.';
$string['Folders'] = 'مجلدات';
$string['foldernotempty'] = 'هذا المجلد ليس فارغاً.';
$string['foldercreated'] = 'تم إنشاء المجلد';
$string['foldernamerequired'] = 'يرجى إعطاء اسم للمجلد الجديد.';
$string['gotofolder'] = 'الذهاب إلى %s';
$string['groupfiles'] = 'ملفات المجموعة';
$string['home'] = 'الصفحة الرئيسية';
$string['htmlremovedmessage'] = 'أنت تشاهد <strong>%s</strong> حسب <a href="%s">%s</a>. لقد تم ترشيح الملف المعروض أدناه لحذف المحتوى المزيف، و هو فقط عبارة عن صورة خام من الأصلية.';
$string['htmlremovedmessagenoowner'] = 'أنت تشاهد <strong>%s</strong> <a href="%s">%s</a>. لقد تم ترشيح الملف المعروض أدناه لحذف المحتوى المزيف، و هو فقط عبارة عن صورة خام من الأصلية.';
$string['image'] = 'صورة';
$string['lastmodified'] = 'آخر تعديل';
$string['myfiles'] = 'ملفاتي';
$string['Name'] = 'الاسم';
$string['namefieldisrequired'] = 'حقل الاسم إلزامي';
$string['maxuploadsize'] = 'الحد الأقصى لحجم التحميل';
$string['movefaileddestinationinartefact'] = 'لا يمكنك وضع مجلد داخل نفسه.';
$string['movefaileddestinationnotfolder'] = 'يمكنك نقل ملفات فقط داخل مجلدات.';
$string['movefailednotfileartefact'] = 'يمكن نقل أدوات صورة، مجلد و ملف فقط.';
$string['movefailednotowner'] = 'لا يوجد لديك إذن بنقل هذا ملف داخل هذا المجلد';
$string['movefailed'] = 'تعذر النقل.';
$string['movingfailed'] = 'تعذر النقل: الملف أو المجلد لم يعد موجوداً';
$string['nametoolong'] = 'ذلك الاسم طويل جداً. يرجى اختيار اسم أقصر.';
$string['nofilesfound'] = 'لم يتم العثور على ملفات';
$string['notpublishable'] = 'لا يوجد لديك إذن بنشر هذا الملف';
$string['overwrite'] = 'استبدال';
$string['Owner'] = 'مالك';
$string['parentfolder'] = 'مجلد أساسي';
$string['Preview'] = 'معاينة';
$string['requireagreement'] = 'Require Agreement';
$string['removingfailed'] = 'تعذر الحذف: الملف أو المجلد لم يعد موجوداً';
$string['savechanges'] = 'حفظ التغييرات';
$string['selectafile'] = 'اختر ملفاً';
$string['selectingfailed'] = 'تعذر الاختيار: الملف أو المجلد لم يعد موجوداًُ';
$string['Size'] = 'الحجم';
$string['spaceused'] = 'المساحة المستخدمة';
$string['timeouterror'] = 'تعذر تحميل الملف: حاول تحميل الملف مرة أخرى';
$string['title'] = 'الاسم';
$string['titlefieldisrequired'] = 'حقل الاسم إلزامي';
$string['Type'] = 'النوع';
$string['upload'] = 'تحميل';
$string['uploadagreement'] = 'اتفاقية تحميل';
$string['uploadagreementdescription'] = 'قم بتفعيل هذا الخيار إذا كنت ترغب بإجبار المستخدمين على الموافقة على النص أدناه قبل أن يتمكنوا من تحميل ملف على الموقع.';
$string['uploadexceedsquota'] = 'سيفوق تحميل هذا الملف مساحة القرص الخاص بك. حاول أن تحذف بعضاالملفات التي قد قمت بتحميلها.';
$string['uploadfile'] =  'تحميل ملف';
$string['uploadfileexistsoverwritecancel'] =  'يوجد ملف يحمل هذا الاسم يمكنك إعادة تسمية الملف الذي أنت على وشك تحميله، أو أن تستبدل الملف الموجود.';
$string['uploadingfiletofolder'] =  'تحميل %s على %s';
$string['uploadoffilecomplete'] = 'اكتمل تحميل %s';
$string['uploadoffilefailed'] =  'تعذر تحميل %s';
$string['uploadoffiletofoldercomplete'] = 'اكتمل تحميل %s على %s';
$string['uploadoffiletofolderfailed'] = 'تعذر تحميل %s على %s';
$string['usecustomagreement'] = 'استخدام اتفاقية مخصصة';
$string['youmustagreetothecopyrightnotice'] = 'يجب أن توافق على إشعار حقوق الطبع';
$string['fileuploadedtofolderas'] = 'تم تحميل %s على %s بصيغة "%s"';
$string['fileuploadedas'] = 'تم تحميل %s بصيغة "%s"';


// File types
$string['ai'] = 'مستند Postscript';
$string['aiff'] = 'ملف صوتي من نوع AIFF';
$string['application'] = 'تطبيق غير معروف';
$string['au'] = 'ملف صوتي من نوع AU';
$string['avi'] = 'ملف مرئي من نوع AVI';
$string['bmp'] = 'صورة من نوع Bitmap';
$string['doc'] = 'مستند من نوع MS Word';
$string['dss'] = 'ملف صوتي من نوع مقياس التحدث الرقمي';
$string['gif'] = 'صورة من GIF';
$string['html'] = 'ملف من نوع HTML';
$string['jpg'] = 'صورة من نوع JPEG';
$string['jpeg'] = 'صورة من نوع JPEG';
$string['js'] = 'ملف من نوع Javascript';
$string['latex'] = 'مستند من نوع LaTeX';
$string['m3u'] = 'ملف صوتي من نوع M3U';
$string['mp3'] = 'ملف صوتي من نوع MP3';
$string['mp4_audio'] = 'ملف صوتي من نوع MP4';
$string['mp4_video'] = 'ملف مرئي من نوع MP4';
$string['mpeg'] = 'ملف مرئي من نوع MPEG';
$string['odb'] = 'قاعدة بيات Openoffice';
$string['odc'] = 'ملف حسابي Openoffice';
$string['odf'] = 'ملف معادلة Openoffice';
$string['odg'] = 'ملف جرافيكي Openoffice';
$string['odi'] = 'صورة من نوع Openoffice';
$string['odm'] = 'Openoffice Master Document File';
$string['odp'] = 'عرض تقديمي Presentation';
$string['ods'] = 'جدول ممتد من نوع Openoffice';
$string['odt'] = 'مستند من نوع Openoffice';
$string['oth'] = 'مستند ويب من نوع Openoffice';
$string['ott'] = 'Openoffice مستند قالب من نوع Openoffice';
$string['pdf'] = 'مستند من نوع PDF';
$string['png'] = 'صورة من نوع PNG';
$string['ppt'] = 'مستند من نوع MS Powerpoint';
$string['quicktime'] = 'ملف مرئي من نوع Quicktime';
$string['ra'] = 'ملف صوتي من نوع Real';
$string['rtf'] = 'مستند من نوع RTF';
$string['sgi_movie'] = 'ملف مرئي من نوع SGI';
$string['sh'] = 'Shell Script';
$string['tar'] = 'أرشيف TAR';
$string['gz'] = 'ملف مضغوط من نوع Gzip';
$string['bz2'] = 'ملف مضغوط من نوع Bzip2';
$string['txt'] = 'ملف نص عادي';
$string['wav'] = 'ملف صوتي من نوع WAV';
$string['wmv'] = 'ملف مرئي من نوع WMV';
$string['xml'] = 'ملف XML';
$string['zip'] = 'أرشيف ZIP';
$string['swf'] = 'ملف مرئي من نوع SWF Flash';
$string['flv'] = 'ملف مرئي من نوع FLV Flash';
$string['mov'] = 'ملف مرئي من نوع MOV Quicktime';
$string['mpg'] = 'ملف مرئي من نوع MPG';
$string['ram'] = 'ملف مرئي من نوع RAM Real Player';
$string['rpm'] = 'ملف مرئي من نوع RPM Real Player';
$string['rm'] = 'ملف مرئي من نوع RM Real Player';


// Profile icons
$string['cantcreatetempprofileiconfile'] = 'تعذر نسخ صورة مؤقتة لأيقونة الصفحة الشخصية في %s';
$string['profileiconsize'] = 'حجم أيقونة الصفحة الشخصية';
$string['profileicons'] = 'أيقونات الصفحة الشخصية';
$string['Default'] = 'افتراضي';
$string['deleteselectedicons'] = 'حذف الأيقونات المختارة';
$string['profileicon'] = 'أيقونة الصفحة الشخصية';
$string['noimagesfound'] = 'لم يتم العثور على صور';
$string['uploadedprofileiconsuccessfully'] = 'تم تحميل أيقونة جديدة للصفحة الشخصية بنجاح';
$string['profileiconsetdefaultnotvalid'] = 'تعذر وضع الأيقونة الافتراضية للصفحة الشخصية, لم يكن الاختيار صحيحاً';
$string['profileiconsdefaultsetsuccessfully'] = 'تم وضع الأيقونة الافتراضية للصفحة الشخصية بنجاح';
$string['profileiconsdeletedsuccessfully'] = 'تم حذف أيقونة/أيقونات الصفحة الشخصية بنجاح';
$string['profileiconsnoneselected'] = 'لم يتم اختيار أيقونات الصفحة الشخصية المراد حذفها';
$string['onlyfiveprofileicons'] = 'يمكنك تحميل خمسة أيقونات للصفحة الشخصية فقط';
$string['or'] = 'أو';
$string['profileiconuploadexceedsquota'] = 'يمكن أن يفوق تحميل أيقونة الصفحة الشخصية هذه مساحة التخزين الخاصة بك. حاول أن تحذف بعض الملفات التي قد قمت بتحميلها';
$string['profileiconimagetoobig'] = 'الصورة التي قمت بتحميلها كبيرة جداً (%sx%s بكسل). يجب أن لا تكون أكبر من  %sx%s بكسل';
$string['uploadingfile'] = 'جاري تحميل الملف...';
$string['uploadprofileicon'] = 'حمِّل أيقونة الصفحة الشخصية';
$string['profileiconsiconsizenotice'] = 'يمكنك أن تحمل ما يصل إلى <strong>خمسة</strong> أيقونات للصفحة الشخصية هنا, و أن تختار واحدة من الأيقونات ليتم عرضها على أنها الأيقونة الافتراضية في أي وقت تريد. يجب أن يكون حجم أيقونتك ما بين 16*16 و  %sx%s بكسل.';
$string['setdefault'] = 'اجعلها افتراضية';
$string['Title'] = 'العنوان';
$string['imagetitle'] = 'عنوان الصورة';
$string['usenodefault'] = 'لا تستخدم صورة افتراضية';
$string['usingnodefaultprofileicon'] = 'الآن لا يوجد لديك أيقونة افتراضية للصفحة الشخصية';
$string['wrongfiletypeforblock'] = 'الملف الذي قمت بتحميله لم يكن النوع الصحيح لهذا الصندوق.';

// Unzip
$string['Contents'] = 'محتويات';
$string['Continue'] = 'متابعة';
$string['extractfilessuccess'] = 'تم إنشاء %s مجلدات و %s ملفات.';
$string['filesextractedfromarchive'] = 'تم استخراج الملفات من الأرشيف';
$string['filesextractedfromziparchive'] = 'تم استخراج الملفات من أرشيف Zip';
$string['fileswillbeextractedintofolder'] = 'سيتم استخراج الملفات داخل %s';
$string['insufficientquotaforunzip'] = 'كوتا الملف المتبقية لك صغيرة جداً لفك ضغط هذا الملف.';
$string['invalidarchive'] = 'خطأ في قرائة ملف الأرشيف.';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'يرجى الانتظار بينما تتم عملية فك ضغط ملفاتك.';
$string['spacerequired'] = 'المساحة المطلوبة';
$string['Unzip'] = 'فك ضغط';
$string['unzipprogress'] = 'تم إنشاء %s ملفات/مجلدات.';
?>
