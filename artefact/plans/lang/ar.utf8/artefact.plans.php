<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

/* Plans */
$string['description'] = 'الوصف';
$string['deleteplanconfirm'] = 'هل أنت متاكد من أنك ترغب بحذف هذه الخطة؟ لأن حذف هذه الخطة سيؤدي إلى حذف المهام التي تحويها';
$string['deleteplan'] = 'حذف الخطة';
$string['deletethisplan'] = 'حذف الخطة: \'%s\'';
$string['editplan'] = 'تعديل الخطة';
$string['editingplan'] = 'تعديل الخطة';
$string['managetasks'] = 'إدارة المهام';
$string['myplans'] = 'خططي';
$string['newplan'] = 'خطة جديدة';
$string['noplansaddone'] = 'لا يوجد خطط بعد. %sإضافة خطة%s!';
$string['noplans'] = 'لا يوجد خطط للعرض';
$string['plan'] = 'الخطة';
$string['plans'] = 'الخطط';
$string['plandeletedsuccessfully'] = 'تم حذف الخطة بنجاح.';
$string['plannotdeletedsuccessfully'] = 'يوجد خطأ في حذف الخطة.';
$string['plannotsavedsuccessfully'] = 'يوجد خطأ في إرسال هذا النموذج. الرجاء التأكد من الحقول المحددة و المحاولة مرة أخرى.';
$string['plansavedsuccessfully'] = 'تم حفظ الخطة بنجاح.';
$string['planstasks'] = 'Plan \'%s\' tasks.';
$string['planstasksdesc'] = 'قم بإضافة مهام أدناه أو باستخدام الزر الموجود على اليمين ابدأ بعمل خطتك.';
$string['saveplan'] = 'حفظ الخطة';
$string['title'] = 'العنوان';
$string['titledesc'] = 'سيتم استخدام العنوان لعرض كل مهمة في صندوق خططي.';

/* Tasks */
$string['alltasks'] = 'جميع المهام';
$string['completed'] = 'مكتمل';
$string['completiondate'] = 'تاريخ الإتمام';
$string['completeddesc'] = 'قم بالتأشير على مهمتك على أنها مكتملة.';
$string['deletetaskconfirm'] = 'هل أنت متأكد من أنك ترغب بحذف هذه المهمة';
$string['deletetask'] = 'حذف المهمة';
$string['deletethistask'] = 'حذف المهمة: \'%s\'';
$string['edittask'] = 'تعديل المهمة';
$string['editingtask'] = 'جاري تعديل المهمة';
$string['mytasks'] = 'مهامي';
$string['newtask'] = 'مهمة جديدة';
$string['notasks'] = 'لا يوجد مهام للعرض.';
$string['notasksaddone'] = 'لا يوجد مهام بعد. %sإضافة مهمة%s!';
$string['savetask'] = 'حفظ المهمة';
$string['task'] = 'مهمة';
$string['tasks'] = 'مهام';
$string['taskdeletedsuccessfully'] = 'تم حذف المهمة بنجاح.';
$string['tasksavedsuccessfully'] = 'تم حفظ المهمة بنجاح.';


?>
