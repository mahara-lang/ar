<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'المدونات';

$string['blog'] = 'مدونة';
$string['blogs'] = 'المدونات';
$string['addblog'] = 'إنشاء مدونة';
$string['addpost'] = 'مشاركة جديدة';
$string['alignment'] = 'محاذاة';
$string['allowcommentsonpost'] = 'السماح بالتعليقات على مشاركتك.';
$string['allposts'] = 'جميع المشاركات';
$string['attach'] = 'إرفاق';
$string['attachedfilelistloaded'] = 'تم تحميل قائمة الملف المرفق';
$string['attachedfiles'] = 'الملفات المرفقة';
$string['attachment'] = 'مرفق';
$string['attachments'] = 'مرفقات';
$string['blogcopiedfromanotherview'] = 'ملاحظة: لقد تم نسخ هذا الصندوق من صفحة إلكترونية أخرى. يمكنك نقله أو حذفه، و لكن لا يمكنك حذف ما %s بداخله.';
$string['blogdesc'] = 'وصف';
$string['blogdescdesc'] = 'e.g., ‘A record of Jill\'s experiences and reflections’.';
$string['blogdoesnotexist'] = 'أنت تحاول الوصول إلى مدونة غير موجودة';
$string['blogpostdoesnotexist'] = 'أنت تحاول الوصول إلى تدوينة غير موجودة';
$string['blogpost'] = 'تدوينة';
$string['blogdeleted'] = 'تم حذف المدونة';
$string['blogpostdeleted'] = 'تم حذف التدوينة';
$string['blogpostpublished'] = 'تم نشر التدوينة';
$string['blogpostsaved'] = 'تم حفظ التدوينة';
$string['blogsettings'] = 'إعدادات المدونة';
$string['blogtitle'] = 'العنوان';
$string['blogtitledesc'] = 'e.g., ‘Jill’s Nursing Practicum Journal’.';
$string['border'] = 'إطار';
$string['cancel'] = 'إلغاء';
$string['createandpublishdesc'] = 'بهذا سيتم إنشاء التدوينة و جعلها متوفرة للآخرين.';
$string['createasdraftdesc'] = 'بهذا سيتم إنشاء التدوينة، و لكنها لن تصبح متوفرة للآخرين حتى تختار أن تقوم بنشرها.';
$string['createblog'] = 'إنشاء المدونة';
$string['dataimportedfrom'] = 'تم استيراد البيانات من %s';
$string['defaultblogtitle'] = '%s\'s مدونة';
$string['delete'] = 'حذف';
$string['deleteblog?'] = 'هل أنت متأكد من أنك تريد هذه المدونة؟';
$string['deleteblogpost?'] = 'هل أنت متأكد من أنك تريد هذه المشاركة؟';
$string['description'] = 'وصف';
$string['dimensions'] = 'أبعاد';
$string['draft'] = 'مسودة';
$string['edit'] = 'تعديل';
$string['editblogpost'] = 'تعديل التدوينة';
$string['entriesimportedfromleapexport'] = 'تم استيراد المدخلات من تصدير LEAP، حيث تعذر استيرادها من كان آخر.';
$string['errorsavingattachments'] = 'حدث خطأ أثناء عملية حفظ مرفقات التدوينة';
$string['horizontalspace'] = 'الفراغ الإفقي';
$string['insert'] = 'إدخال';
$string['insertimage'] = 'إدخال صورة';
$string['moreoptions'] = 'خيارات أكثر';
$string['mustspecifytitle'] = 'يجب أن تحدد عنواناً لمشاركتك';
$string['mustspecifycontent'] = 'يجب أن تحدد بعض المحتويات لمشاركتك';
$string['myblogs'] = 'مدوناتي';
$string['myblog'] = 'مدونتي';
$string['name'] = 'الاسم';
$string['newattachmentsexceedquota'] = 'يفوق الحجم الكلي للملفات الجديدة التي قمت بتحميلها إلى هذه المشاركة الكوتا الخاصة بك. يمكنك حفظ المشاركة إذا قمت بحذف بعض المرفقات التي قمت بإضافتها تواً.';
$string['newblog'] = 'مدونة جديدة';
$string['newblogpost'] = 'تدوينة جديدة في المدونة "%s"';
$string['newerposts'] = 'مشاركات أحدث';
$string['nopostsyet'] = 'لا يوجد مشاركات بعد.';
$string['addone'] = 'أضف مشاركة!';
$string['noimageshavebeenattachedtothispost'] = 'لا يوجد صور مرفقة مع هذه المشاركة.  يستلزم أن تحمل أو ترفق صورة مع المشاركة قبل أن تقوم بإداخالها.';
$string['nofilesattachedtothispost'] = 'لا يوجد ملفات مرفقة';
$string['noresults'] = 'لم يتم العثور على تدوينات';
$string['olderposts'] = 'مشاركات أقدم';
$string['post'] = 'مشاركة';
$string['postbody'] = 'Body';
$string['postbodydesc'] = ' ';
$string['postedon'] = 'تم نشره بتاريخ';
$string['postedbyon'] = 'قام بنشره %s بتاريخ %s';
$string['posttitle'] = 'العنوان';
$string['posts'] = 'منشورات';
$string['publish'] = 'نشر';
$string['publishfailed'] = 'حدث خطأ. لم يتم نشر منشورك';
$string['publishblogpost?'] = 'هل أنت متاكد من أنك تريد نشر هذا المنشور؟';
$string['published'] = 'تم نشره';
$string['remove'] = 'حذف';
$string['save'] = 'حفظ';
$string['saveandpublish'] = 'حفظ و نشر';
$string['saveasdraft'] = 'حفظ كمسودة';
$string['savepost'] = 'حفظ المنشور';
$string['savesettings'] = 'حفظ الإعدادات';
$string['settings'] = 'الإعدادات';
$string['thisisdraft'] = 'هذا المنشور مسودة';
$string['thisisdraftdesc'] = 'عندما يكون منشورك مسودة، لا يمكن لأحد غيرك مشاهدته.';
$string['title'] = 'العنوان';
$string['update'] = 'تحديث';
$string['verticalspace'] = 'مساحة عمودية';
$string['viewblog'] = 'عرض المدونة';
$string['youarenottheownerofthisblog'] = 'أنت لست مالك هذه المدونة ';
$string['youarenottheownerofthisblogpost'] = 'أنت لست مالك هذه التدوينة';
$string['cannotdeleteblogpost'] = 'حدث خطأ في حذف هذه التدوينة.';

$string['baseline'] = 'خط أساسي';
$string['top'] = 'أعلى';
$string['middle'] = 'وسط';
$string['bottom'] = 'أسفل';
$string['texttop'] = 'Text top';
$string['textbottom'] = 'Text bottom';
$string['left'] = 'يسار';
$string['right'] = 'يمين';
$string['src'] = 'الصورة URL';
$string['image_list'] = 'صورة مرفقة';
$string['alt'] = 'وصف';

$string['copyfull'] = 'سيحصل الآخرون على نسختهم الخاصة من %s الخاص بك';
$string['copyreference'] = 'يمكن أن يعرض الآخرين %s الخاص بك في صفحتهم الإلكترونية';
$string['copynocopy'] = 'تخطى هذا الصندوق بالكامل عند نسخ الصفحة الإلكترونية';

$string['viewposts'] = 'المنشورات المنسوخة (%s)';
$string['postscopiedfromview'] = 'المنشورات المنسوخة من %s';

$string['youhavenoblogs'] = 'لا يوجد لديك مدونات.';
$string['youhaveoneblog'] = 'لديك  مدونة واحدة.';
$string['youhaveblogs'] = 'لديك %s مدونات.';

$string['feedsnotavailable'] = 'التعليقات غير متوفرة لهذا النوع من الطبقات.';
$string['feedrights'] = 'حقوق الطبع %s.';

$string['enablemultipleblogstext'] = 'لديك مدونة واحدة. إذا كنت ترغب ببدء مدونة جديدة، قم بتفعيل خيار مدونات متعددة على صفحة  <a href="%saccount/">إعدادات الحساب</a>.';
?>
