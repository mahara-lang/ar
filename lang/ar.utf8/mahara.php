<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// General form strings
$string['add']     = 'إضافة';
$string['cancel']  = 'إلغاء';
$string['delete']  = 'حذف';
$string['edit']    = 'تعديل';
$string['editing'] = 'تعديل';
$string['save']    = 'حفظ';
$string['submit']  = 'إرسال';
$string['update']  = 'تحديث';
$string['change']  = 'تغيير';
$string['send']    = 'إرسال';
$string['go']      = 'ذهاب';
$string['default'] = 'افتراضي';
$string['upload']  = 'تحميل';
$string['complete']  = 'إكمال';
$string['Failed']  = 'فشل';
$string['loading'] = 'جاري التحميل ...';
$string['showtags'] = 'أظهر أوسمتي';
$string['errorprocessingform'] = 'يوجد خطأ في إرسال هذا النموذج. الرجاء التحقق من الحقول المحددة و المحاولة مرة أخرى.';
$string['description'] = 'وصف';
$string['remove']  = 'حذف';
$string['Close'] = 'إغلاق';
$string['Help'] = 'المساعدة';
$string['applychanges'] = 'تطبيق التغييرات';

$string['no']     = 'لا';
$string['yes']    = 'نعم';
$string['none']   = 'None';
$string['at'] = 'at';
$string['From'] = 'من';
$string['To'] = 'إلى';
$string['All'] = 'الكل';

$string['enable'] = 'تفعيل';
$string['disable'] = 'تعطيل';
$string['pluginenabled'] = 'تم تفعيل البرنامج المساعد';
$string['plugindisabled'] = 'تم تعطيل البرنامج المساعد';
$string['pluginnotenabled'] = 'البرنامج غير مفعل.  عليك أن تفعل %s البرنامج المساعد أولاً.';

$string['next']      = 'التالي';
$string['nextpage']  = 'الصفحة التالية';
$string['previous']  = 'السابق';
$string['prevpage']  = 'الصفحة السابقة';
$string['first']     = 'الأول';
$string['firstpage'] = 'الصفحة الأولى';
$string['last']      = 'الأخير';
$string['lastpage']  = 'الصفحة الأخيرة';

$string['accept'] = 'قبول';
$string['memberofinstitutions'] = 'Member of %s';
$string['reject'] = 'رفض';
$string['sendrequest'] = 'إرسال الطلب';
$string['reason'] = 'السبب';
$string['select'] = 'اختيار';

// Tags
$string['tags'] = 'Tags';
$string['tagsdesc'] = 'أدخل أوسمة مفصولة بفواصل لهذا العنصر.';
$string['tagsdescprofile'] = 'أدخل أوسمة مفصولة بفواصل لهذا العنصر. العناصر الموسومة بـ \'profile\' يتم عرضها في العمود الجانبي الخاص بك.';
$string['youhavenottaggedanythingyet'] = 'لم تقم بوسم أي شيء حتى الآن';
$string['mytags'] = 'أوسمتي';
$string['Tag'] = 'وسم';
$string['itemstaggedwith'] = 'العناصر الموسومة بـ "%s"';
$string['numitems'] = '%s عناصر';
$string['searchresultsfor'] = 'نتائج البحث لـ';
$string['alltags'] = 'جميع الأوسمة';
$string['sortalpha'] = 'ترتيب الأسمة أبجدياً';
$string['sortfreq'] = 'ترتيب الأوسمة حسب التكرار';
$string['sortresultsby'] = 'ترتيب النتائج حسب:';
$string['date'] = 'التاريخ';
$string['dateformatguide'] = 'استخدم الصيغة يوم/شهر/سنة';
$string['datetimeformatguide'] = 'استخدم الصيغة يوم/شهر/سنة ساعة:دقيقة';
$string['filterresultsby'] = 'ترشيح النتائج حسب:';
$string['tagfilter_all'] = 'الكل';
$string['tagfilter_file'] = 'ملفات';
$string['tagfilter_image'] = 'صور';
$string['tagfilter_text'] = 'نص';
$string['tagfilter_view'] = 'الصفحات الإلكترونية';
$string['edittags'] = 'تعديل الأوسمة';
$string['selectatagtoedit'] = 'اختر وسماً للتعديل';
$string['edittag'] = 'تعديل <a href="%s">%s</a>';
$string['editthistag'] = 'تعديل هذا الوسم';
$string['edittagdescription'] = 'سيتم تحديث جميع العناصر في ملفك الشخصي الموسومة بـ "%s"';
$string['deletetag'] = 'حذف <a href="%s">%s</a>';
$string['confirmdeletetag'] = 'هل فعلاً ترغب بحذف هذا الوسم من كل شيء في ملفك الشخصي؟';
$string['deletetagdescription'] = 'احذف هذا الوسم من جميع العناصر في ملفك الشخصي';
$string['tagupdatedsuccessfully'] = 'تم تحديث الوسم بنجاح';
$string['tagdeletedsuccessfully'] = 'تم حذف الوسم بنجاح';

$string['selfsearch'] = 'ابحث في ملفي الشخصي';

// Quota strings
$string['quota'] = '‎ حُصَّة';
$string['quotausage'] = 'لقد استخدمت <span id="quota_used">%s</span> من <span id="quota_total">%s</span> ‎ حُصَّتك.';

$string['updatefailed'] = 'تعذر التحديث';

$string['strftimenotspecified']  = 'غير محدد';

// profile sideblock strings
$string['invitedgroup'] = 'دعوة المجموعة';
$string['invitedgroups'] = 'دعوات المجموعة';
$string['logout'] = 'خروج';
$string['pendingfriend'] = 'صديق معلق';
$string['pendingfriends'] = 'الأصدقاء المعلقين';
$string['profile'] = 'الصفحة الشخصية';
$string['views'] = 'الصفحات الإلكترونية';

// Online users sideblock strings
$string['onlineusers'] = 'المستخدمون المتواجدون حالياً';
$string['lastminutes'] = 'آخر %s دقائق';

// Links and resources sideblock
$string['linksandresources'] = 'الروابط و الموارد';

// auth
$string['accesstotallydenied_institutionsuspended'] = 'لقد تم تعطيل مؤسستك %s.  إلى أن يتم تفعيلها لن تتمكن من تسجيل الدخول إلى %s.
الرجاء الاتصال بمؤسستك للمساعدة.';
$string['accessforbiddentoadminsection'] = 'لا يسمح الوصول إلى قسم الإدارة';
$string['accountdeleted'] = 'عذراً، لقد تم حذف حسابك';
$string['accountexpired'] = 'عذراً، لقد أنتهت صلاحية حسابك';
$string['accountcreated'] = '%s: حساب جديد';
$string['accountcreatedtext'] = 'عزيزي %s,

لقد تم إنشاء حساب جديد لك على %s. تفاصيلك هي كالتالي :

اسم المستخدم: %s
كلمة المرور: %s

قم بزيارة %s للبدىء!

أفضل التحيات، إدارة الموقع';
$string['accountcreatedchangepasswordtext'] = 'عزيزي %s,

لقد تم إنشاء حساب جديد لك بتاريخ %s. تفاصيلك هي كالتالي:

اسم المستخدم: %s
كلمة المرور: %s

عندما تسجل الدخول للمرة الأولى، سيُطلب منك تغيير كلمة المرور.

تفضل بزيارة %s لتباشر بالعمل!

أفضل التحيات، إدارة الموقع';
$string['accountcreatedhtml'] = '<p>عزيزي %s</p>

<p>لقد تم إنشاء حساب جديد لك على <a href="%s">%s</a>. تفاصيلك هي كالتالي:</p>

<ul>
    <li><strong>اسم المستخدم:</strong> %s</li>
    <li><strong>كلمة المرور:</strong> %s</li>
</ul>

<p>تفضل بزيارة <a href="%s">%s</a> لتباشر بالعمل!</p>

<p>أفضل التحيات، %s إدارة الموقع</p>
';
$string['accountcreatedchangepasswordhtml'] = '<p>عزيزي %s</p>

<p>لقد تم إنشاء حساب جديد لك على <a href="%s">%s</a>. تفاصيلك هي كالتالي:</p>

<ul>
    <li><strong>اسم المستخدم:</strong> %s</li>
    <li><strong>كلمة المرور:</strong> %s</li>
</ul>

<p>عندما تسجل الدخول للمرة الأولى، سيُطلب منك تغيير كلمة المرور.</p>

<p>تفضل بزيارة <a href="%s">%s</a> لتباشر بالعمل!</p>

<p>أفضل التحيات، %s إدارة الموقع</p>
';
$string['accountexpirywarning'] = 'تنبيه بانتهاء صلاحية الحساب';
$string['accountexpirywarningtext'] = 'عزيزي %s,

حسابك على %s سينتهي خلال الفترة %s.

ننصحك بأن تحفظ محتويات ملفك الشخصي باستخدام أداة التصدير. يمكنك العثور على تعليمات حول استخدام هذه الميزة داخل دليل المستخدم.

إذا كنت ترغب بتوسيع صلاحية الوصول إلى حسابك أو لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في الاتصال بنا:

%s

أفضل التحيات، %s إدارة الموقع';
$string['accountexpirywarninghtml'] = '<p>عزيزي %s,</p>
    
<p>حسابك على %s سينتهي خلال %s.</p>

<p>نوصيك بأن تحفظ محتويات ملفك الشخصي باستخدام أداة التصدير. يمكن العثور على تعليمات حول استخدام هذه الميزة في دليل المستخدم.</p>

<p>إذا كنت ترغب بتوسيع صلاحية الوصول إلى حسابك أو لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في  <a href="%s">الاتصال بنا</a>.</p>

<p>أفضل التحيات، %s مدير الموقع</p>';
$string['institutionmembershipexpirywarning'] = 'تنبيه بانتهاء عضوية المؤسسة';
$string['institutionmembershipexpirywarningtext'] = 'عزيزي %s،

عضويتك في %s على %s ستنتهي خلال %s.

إذا كنت ترغب بتوسيع صلاحية الوصول إلى حسابك أو لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في الاتصال بنا:

%s

أفضل التحيات، %s مدير الموقع';
$string['institutionmembershipexpirywarninghtml'] = '<p>عزيزي %s،</p>

<p>عضويتك في %s على %s ستنتهي خلال %s.</p>

<p>إذا كنت ترغب بتوسيع صلاحية الوصول إلى حسابك أو لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في  <a href="%s">الاتصال بنا</a>.</p>

<p>أفضل التحيات، %s مدير الموقع</p>';
$string['institutionexpirywarning'] = 'تنبيه بانتهاء عضوية المؤسسة';
$string['institutionexpirywarningtext_institution'] = 'عزيزي %s،

%s\'s membership of %s will expire within %s.

If you wish to extend your institution\'s membership or have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['institutionexpirywarninghtml_institution'] = '<p>Dear %s,</p>

<p>%s\'s membership of %s will expire within %s.</p>

<p>إذا كنت ترغب بتوسيع عضوية مؤسستك أو لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في  <a href="%s">الاتصال بنا</a>.</p>

<p>أفضل التحيات، %s مدير الموقع</p>';
$string['institutionexpirywarningtext_site'] = 'عزيزي %s،

ستنتهي المؤسسة \'%s\' خلال %s .

You may wish to contact them to extend their membership of %s.

أفضل التحيات، %s مدير الموقع';
$string['institutionexpirywarninghtml_site'] = '<p>عزيزي %s،</p>

<p>ستنتهي المؤسسة \'%s\' خلال %s.</p>

<p>You may wish to contact them to extend their membership of %s.</p>

<p>أفضل التحيات، %s مدير الموقع</p>';
$string['accountinactive'] = 'عذراً، حسابك غير فعال حالياً';
$string['accountinactivewarning'] = 'تنبيه بعدم فعالية الحساب';
$string['accountinactivewarningtext'] = 'عزيزي %s،

سيصبح حسابك على %s غير فعالاً خلال %s.

عندما يصبح حسابك غير فعالاً، لن تكن فادراً على الدخول إلى أن يقوم مدير بتفعيل حسابك.

يمكنك أن تمنع حسابك من أن يصبح غير فعال عن طريق تسجيل الدخول.

أفضل التحيات، %s مدير الموقع';
$string['accountinactivewarninghtml'] = '<p>عزيزي %s،</p>

<p>سيصبح حسابك على %s غير فعال خلال %s.</p>

<p>عندما يصبح حسابك غير فعالاً، لن تكن فادراً على الدخول إلى أن يقوم مدير بتفعيل حسابك.</p>

<p>يمكنك أن تمنع حسابك من أن يصبح غير فعال عن طريق تسجيل الدخول.</p>

<p>أفضل التحيات، %s مدير الموقع</p>';
$string['accountsuspended'] = 'لقد تم إيقاف حسابك بتاريخ %s. سبب إيقاف حسابك هو:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = 'لقد تم إيقاف حسابك';
$string['youraccounthasbeenunsuspended'] = 'لقد تم تفعيل حسابك';
$string['changepassword'] = 'تغيير كلمة المرور';
$string['changepasswordinfo'] = 'يجب عليك أن تغير كلمة المرور قبل أن تتمكن من المتابعة.';
$string['chooseusernamepassword'] = 'اختر اسم المستخدم و كلمة المرور خاصيك';
$string['chooseusernamepasswordinfo'] = 'تحتاح اسم مستخدم و كلمة مرور حتى تتمكن من تسجيل الدخول إلى %s.  يرجى اختيارهما الآن.';
$string['confirmpassword'] = 'تأكيد كلمة المرور';
$string['javascriptnotenabled'] = 'لا يوجد لدى متصفحك javascript فعال لهذا الموقع. يتطلب مهارة أن يكون javascript فعالاً قبل أن تتمكن من تسجيل الدخول ';
$string['cookiesnotenabled'] = 'لا يوجد لدى متصفحك ملفات تعريف ارتباط من هذا الموقع. يتطلب مهارة أن تكون ملفات تعريف الارتباط فعالة قبل أن تتمكن من تسجيل الدخول';
$string['institution'] = 'مؤسسة';
$string['loggedoutok'] = 'لقد قمت بتسجيل الخروج بنجاح';
$string['login'] = 'تسجيل الدخول';
$string['loginfailed'] = 'لم تكن معلومات الدخول التي قمت بإعطائها صحيحة. يرجى التحقق فيما إذا كان اسم المستخدم و كلمة المرور صحيحان.';
$string['loginto'] = 'تسجيل الدخول إلى %s';
$string['newpassword'] = 'كلمة المرور الجديدة';
$string['nosessionreload'] = 'إعادة تحميل الصفحة لتسجيل الدخول';
$string['oldpassword'] = 'كلمة المرور الحالية';
$string['password'] = 'كلمة المرور';
$string['passworddescription'] = ' ';
$string['passwordhelp'] = 'كلمة المرور التي تستخدمها للوصول إلى النظام';
$string['passwordnotchanged'] = 'لم تغير كلمة المرور خاصتك، يرجى اختيار كلمة مرور جديدة';
$string['passwordsaved'] = 'لقد تم حفظ كلمة مرورك الجديدة';
$string['passwordsdonotmatch'] = 'كلمات المرور لا تتطابق';
$string['passwordtooeasy'] = 'كلمة المرور الخاصة بك سهلة جداً! يرجى اختيار كلمة مرور أصعب';
$string['register'] = 'التسجيل';
$string['sessiontimedout'] = 'لقد انتهت صلاحية تصفحك، يرجى إدخال معلومات الدخول الخاصة بك للمتابعة';
$string['sessiontimedoutpublic'] = 'لقد انتهت صلاحية تصفحك. يمكنك <a href="%s">تسجيل الدخول</a> لمتابعة التصفح';
$string['sessiontimedoutreload'] = 'لقد انتهت صلاحية تصفحك. قم بإعادة تحميل الصفحة لتدخل مرة اخرى';
$string['username'] = 'اسم المستخدم';
$string['preferredname'] = 'اسم العرض';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = 'اسم المستخدم الذي لك لكي تتمكن من الوصول إلى النظام.';
$string['youaremasqueradingas'] = 'أنت تتنكر باسم %s.';
$string['yournewpassword'] = 'كلمة المرور الجديدة الخاصة بك. يجب أن يبلغ طول كلمات المرور 6 رموز على الأقل و أن تحتوي على رقم واحد و حرفين على الأقل';
$string['yournewpasswordagain'] = 'كلمة المرور الخاصة بك مرة أخرى';
$string['invalidsesskey'] = 'Invalid session key';
$string['cannotremovedefaultemail'] = 'لا يمكنك حذف عنوان بريدك الإلكتروني الرئيسي';
$string['emailtoolong'] = 'لا يمكن أن يفوق عدد حروف عناوين البريد الإلكتروني 255 حرف';
$string['mustspecifyoldpassword'] = 'يجب أن تحدد كلمة المرور الحالية الخاصة بك';
$string['Site'] = 'موقع';

// Misc. register stuff that could be used elsewhere
$string['emailaddress'] = 'عنوان البريد الإلكتروني';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = 'الاسم الأول';
$string['firstnamedescription'] = ' ';
$string['lastname'] = 'الاسم الأخير';
$string['lastnamedescription'] = ' ';
$string['studentid'] = 'الرقم التعريفي';
$string['displayname'] = 'اسم العرض';
$string['fullname'] = 'الاسم بالكامل';
$string['registerwelcome'] = 'أهلاً بك! لتستخدم هذا الموقع عليك التسجيل أولاً.';
$string['registeragreeterms'] = 'و يجب عليك أيضا أن توافق على<a href="terms.php">الشروط و الأحكام</a>.';
$string['registerprivacy'] = 'سيتم تخزين البيانات التي نجمعها وفقاً <a href="privacy.php">لبيان الخصوصية الخاص بنا</a>.';
$string['registerstep3fieldsoptional'] = '<h3>اختر صورة اختيارية للملف الشخصي</h3><p>لقد قمت الآن بالتسجيل بنجاح مع %s! يمكنك اآن اختيار أيقونة اختيارية للملف الشخصي ليتم عرضها كشعار لك.</p>';
$string['registerstep3fieldsmandatory'] = '<h3>قم بتعبئة حقول الملف الشخصي الإلزامية</h3><p>الحقول التالية إلزامية. و يجب عليك تعبئة هذه الحقول قبل أن يكتمل تسجيلك.</p>';
$string['registeringdisallowed'] = 'عذراً، لا يمكنك التسجيل في هذا النظام في هذا الوقت';
$string['membershipexpiry'] = 'تنتهي العضوية';
$string['institutionfull'] = 'المؤسسة التي قد قمن باختيارها لا تستوعب المزيد من عمليات التسجيل.';
$string['registrationnotallowed'] = 'المؤسسة التي قد قمت باختيارها لا تسمح بالتسجيل الذاتي.';
$string['registrationcomplete'] = 'شكراً لتسجيلك في %s';
$string['language'] = 'اللغة';

// Forgot password
$string['cantchangepassword'] = 'عذراً، لا يمكنك تغيير كلمة المرور الخاصة بك من خلال هذه النافذه - يرجى استخدام واجهة مؤسستك بدلاً من هذه';
$string['forgotusernamepassword'] = 'هل نسيت اسم المستخدم الخاص أو كلمة المرور؟';
$string['forgotusernamepasswordtext'] = '<p>إذا كنت قد نسيت اسم المستخدم الخاص بك أو كلمة المرور، قم بإدخال عنوان البريد الإلكتروني المدرج في ملفك الشخصي و سنرسل لك رسالة يمكنك استخدامها لتمنح نفسك كلمة مرور جديدة.</p>
<p>إذا كنت تعرف كلمة المرور الخاصة بك و قد نسيتها، يمكنك أيضاً إدخال اسم المستخدم الخاص بك بدلاً من كلمة المرور.</p>';
$string['lostusernamepassword'] = 'فقدان اسم المستخدم أو كلمة المرور';
$string['emailaddressorusername'] = 'عنوان البريد الإلكتروني أو اسم المستخدم';
$string['pwchangerequestsent'] = 'سيصلك بريد إلكتروني قريباً يحتوي على رابط يمكنك استخدامه لتغيير كلمة المرور لحسابك';
$string['forgotusernamepasswordemailsubject'] = 'تفاصيل اسم المستخدم/كلمة المرور لـ %s';
$string['forgotusernamepasswordemailmessagetext'] = 'عزيزي %s،

لقد تم تنفيذ طلب اسم مستخدم/كلمة مرور لحسابك على  %s.

اسم المستخدم الخاص بك هو %s.

إذا كنت ترغب باسترجاع كلمة المرور الخاصة، يرجى اتباع الرابط أدناه:

%s

إذا لم تتطلب استرجاع كلمة مرور، يرجى تجاهل هذا البريد الإلكتروني.

إذا كان لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في الاتصال بنا:

%s

أفضل التحيات، %s مدير الموقع';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>عزيزي %s،</p>

<p>لقد تم تنفيذ طلب اسم مستخدم/كلمة مرور لحسابك على  %s.</p>

<p>اسم المستخدم الخاص بك هو <strong>%s</strong>.</p>

<p>إذا كنت ترغب باسترجاع كلمة المرور الخاصة، يرجى اتباع الرابط أدناه:</p>

<p><a href="%s">%s</a></p>

<p>إذا لم تتطلب استرجاع كلمة مرور، يرجى تجاهل هذا البريد الإلكتروني.</p>

<p>إذا كان لديك أي استفسار بخصوص ما جاء أعلاه، لا تتردد في  <a href="%s">الاتصال بنا</a>.</p>

<p>أفضل التحيات، %s مدير الموقع</p>';
$string['forgotpassemailsendunsuccessful'] = 'عذراً، يبدو أنه لم يتم إرسال البريد الإلكتروني بنجاح. هذا، حذا خطأنا، يرجى المحاولة في أقرب وقت';
$string['forgotpassemaildisabled'] = 'عذراً، البريد الإلكتروني لعنوان البريد الإلكتروني أو اسم المستخدم الذي قمت بإدخاله مُعطل. يرجى الاتصال يمدير لاسترجاع كلمة المرورالخاصة بك.';
$string['forgotpassnosuchemailaddressorusername'] = 'عنوان البريد الإلكتروني أو اسم المستخدم الذي قمت بإدخاله لا يطابق أي من مستخدمي الموقع';
$string['forgotpasswordenternew'] = 'يرجى إدخال كلمة المرور الجديدة الخاصة بك للمتابعة';
$string['nosuchpasswordrequest'] = 'لا يوجد طلب كلمة مرور مثل هذا';
$string['passwordchangedok'] = 'لقد تم تغيير كلمة المرور الخاصة بك بنجاح';

// Reset password when moving from external to internal auth.
$string['noinstitutionsetpassemailsubject'] = '%s: عضوية %s';
$string['noinstitutionsetpassemailmessagetext'] = 'عزيزي %s،

لم تعد عضواً في %s.
يمكنك متابعة استخدام %s باسم المستخدم الخاص بك الحالي %s، و لكن يجب عليك وضع كلمة مرور جديدة لحسابك.

يرجى اتباع الرابط أدناه لمتابعة عملية استرجاع كلمة المرور.

%sforgotpass.php?key=%s

إذا كان لديك استفسار بخصوص ما جاء أعلاه، لا تتردد في الاتصال بنا.

%scontact.php

أفضل التحيات، %s مدير الموقع

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>عزيزي %s،</p>

<p>لم تعد عضواً في %s.</p>
<p>يمكنك متابعة استخدام %s باسم المستخدم الخاص بك الحالي %s، و لكن يجب عليك وضع كلمة مرور جديدة لحسابك.</p>

<p>يرجى اتباع الرابط أدناه لمتابعة عملية استرجاع كلمة المرور.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>إذا كان لديك استفسار يخصوص ما جاء أعلاه، لا تتردد في <a href="%scontact.php">الاتصال بنا</a>.</p>

<p>أفضل الأمنيات %s مدير الموقع</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['debugemail'] = 'ملاحظة: كان هذا البريد الإلكتروني موجه لـ %s <%s> و لكن لقد تم إرساله لك وفقاً لإعداد تهيئة "sendallemailto".';
$string['divertingemailto'] = 'تحويل البريد الإلكتروني إلى %s';


// Expiry times
$string['noenddate'] = 'لا يوجد تاريخ نهاية';
$string['day']       = 'يوم';
$string['days']      = 'أيام';
$string['weeks']     = 'أسابيع';
$string['months']    = 'أشهر';
$string['years']     = 'سنوات';
// Boolean site option

// Site content pages
$string['sitecontentnotfound'] = '%s النص غير متوفر';

// Contact us form
$string['name']                     = 'الاسم';
$string['email']                    = 'البريد الإلكتروني';
$string['subject']                  = 'العنوان';
$string['message']                  = 'الرسالة';
$string['messagesent']              = 'لقد تم إرسال رسالتك';
$string['nosendernamefound']        = 'لم يتم إرسال اسم المرسل';
$string['emailnotsent']             = 'Failed to send contact email. Error message: "%s"';

// mahara.js
$string['namedfieldempty'] = 'الحقل الإلزامي "%s" فارغ';
$string['processing']     = 'جاري المعالجة';
$string['requiredfieldempty'] = 'يوجد حقل إلزامي فارغ';
$string['unknownerror']       = 'حدث خطأ مجهول (0x20f91a0)';

// menu
$string['home']        = 'الصفحة الرئيسية';
$string['myportfolio'] = 'ملفي الشخصي';
$string['myviews']       = ' صفحاتي الإلكترونية';
$string['settings']    = 'الإعدادات';
$string['myfriends']          = 'أصدقائي';
$string['findfriends']        = 'العثور على أصدقاء';
$string['groups']             = 'المجموعات';
$string['mygroups']           = 'مجموعاتي';
$string['findgroups']         = 'العثور على مجموعات';
$string['returntosite']       = 'العودة إلى الموقع';
$string['siteadministration'] = 'ادارة الموقع';
$string['useradministration'] = 'إدارة المستخدم';

$string['unreadmessages'] = 'الرسائل غير المقروئة';
$string['unreadmessage'] = 'الرسائل غير المقروئة';

$string['siteclosed'] = 'الموقع مغلق مؤقتاً من أجل تحديث قاعدة البيانات. يمكن لمديري الموقع تسجيل الدخول.';
$string['siteclosedlogindisabled'] = 'الموقع مغلق مؤقتاً من أجل تحديث قاعدة البيانات.  <a href="%s">إجراء التحديث الآن.</a>';

// footer
$string['termsandconditions'] = 'الشروط و الأحكام';
$string['privacystatement']   = 'بيان الخصوصية';
$string['about']              = 'عن مهارا';
$string['contactus']          = 'اتصل بنا';

// my account
$string['account'] =  'حسابي';
$string['accountprefs'] = 'الفضيلات';
$string['preferences'] = 'التفضيلات';
$string['activityprefs'] = 'تفضيلات النشاط';
$string['changepassword'] = 'تغيير كلمة المرور';
$string['notifications'] = 'إشعارات';
$string['inbox'] = 'صندوق الوارد';
$string['gotoinbox'] = 'اذهب إلى صندوق الوارد';
$string['institutionmembership'] = 'عضوية المؤسسة';
$string['institutionmembershipdescription'] = 'إذا كنت عضواً في أي من المؤسسات، سيتم إدراجها هنا. يمكنك أيضاً عضوية مؤسسات طلب عضوية مؤسسات، و قبول أو رفض دعوات العضوية المعلقة.';
$string['youareamemberof'] = 'أنت عضو في %s';
$string['leaveinstitution'] = 'مغادرة المؤسسة';
$string['reallyleaveinstitution'] = 'هل أنت متأكد من أنك تريد مغادرة هذه المؤسسة؟';
$string['youhaverequestedmembershipof'] = 'لقد طلبت عضوية في %s';
$string['cancelrequest'] = 'إلغاء الطلب';
$string['youhavebeeninvitedtojoin'] = 'لقد تمت دعوتك للانضمام لـ %s';
$string['confirminvitation'] = 'تأكيد الدعوة';
$string['joininstitution'] = 'الانضمام للمؤسسة';
$string['decline'] = 'رفض';
$string['requestmembershipofaninstitution'] = 'طلب عضوية في مؤسسة';
$string['optionalinstitutionid'] = 'الرقم التعريفي للمؤسسة (اختياري)';
$string['institutionmemberconfirmsubject'] = 'تأكيد عضوية مؤسسة';
$string['institutionmemberconfirmmessage'] = 'لقد تمت إضافتك كعضو في %s.';
$string['institutionmemberrejectsubject'] = 'تم رفص طلب عضوية مؤسسة';
$string['institutionmemberrejectmessage'] = 'Your request for membership of %s was declined.';
$string['Memberships'] = 'العضويات';
$string['Requests'] = 'طلبات';
$string['Invitations'] = 'دعوات';

$string['config'] = 'تهيئة';

$string['sendmessage'] = 'إرسال رسالة';
$string['spamtrap'] = 'Spam trap';
$string['formerror'] = 'كان يوجد خطأ في معالجة إرسالك. يرجى المحاولة مرة أخرى.';
$string['formerroremail'] = 'إذا استمرت المشاكل اتصل بنا على %s.';

$string['notinstallable'] = 'غير قابل للتثبيت!';
$string['installedplugins'] = 'البرامج المساعدة المثبتة';
$string['notinstalledplugins'] = 'البرامج المساعدة غير المثبتة';
$string['plugintype'] = 'نوع البرنامج المساعد';

$string['settingssaved'] = 'تم حفظ الإعدادات';
$string['settingssavefailed'] = 'تعذر حفظ الإعدادات';

$string['width'] = 'العرض';
$string['height'] = 'الطول';
$string['widthshort'] = 'w';
$string['heightshort'] = 'h';
$string['filter'] = 'ترشيح';
$string['expand'] = 'توسيع';
$string['collapse'] = 'طي';
$string['more...'] = 'المزيد ...';
$string['nohelpfound'] = 'لم يتم العثور على مساعدة لهذا العنصر';
$string['nohelpfoundpage'] = 'لم يتم العثور على مساعدة لهذه الصفحة';
$string['couldnotgethelp'] = 'حدث خطأ في استرجاع صفحة المساعدة';
$string['profileimage'] = 'صورة الصفحة الشخصية';
$string['primaryemailinvalid'] = 'عنوان بريدك الإلكتروني الرئيسي غير صحيح';
$string['addemail'] = 'إضافة عنوان البريد الإلكتروني';

// Search
$string['search'] = 'البحث';
$string['searchusers'] = 'البحث عن مستخدمين';
$string['Query'] = 'استعلام';
$string['query'] = 'استعلام';
$string['querydescription'] = 'الكلمات المرادالبحث عنها';
$string['result'] = 'نتيجة';
$string['results'] = 'النتائج';
$string['Results'] = 'النتائج';
$string['noresultsfound'] = 'لم يتم العثور على نتائج';
$string['users'] = 'المستخدمون';

// artefact
$string['artefact'] = 'أداة';
$string['Artefact'] = 'أداة';
$string['Artefacts'] = 'أدوات';
$string['artefactnotfound'] = 'لم يتم العثور على الأداة ذات الرقم التعريفي %s';
$string['artefactnotrendered'] = 'لم يتم تصيير الأداة';
$string['nodeletepermission'] = 'لا يوجد لديك إذن بحذف هذه الأداة';
$string['noeditpermission'] = 'لا يوجد لديك إذن بتعديل هذه الأداة';
$string['Permissions'] = 'التراخيص';
$string['republish'] = 'نشر';
$string['view'] = 'صفحة إلكترونية';
$string['artefactnotpublishable'] = 'الأداة %s غير قابلة للنشر في الصفحة الإلكترونية %s';

$string['belongingto'] = 'يخص';
$string['allusers'] = 'جميع المستخدمين';
$string['attachment'] = 'مرفق';

// Upload manager
$string['quarantinedirname'] = 'عزل';
$string['clammovedfile'] = 'لقد تم نقل المجلد إلى مجلد عزل.';
$string['clamdeletedfile'] = 'لقد تم حذف الملف';
$string['clamdeletedfilefailed'] = 'تعذر حذف الملف';
$string['clambroken'] = 'لقد قام مديرك بتفعيل الفحص ضد الفيروس لتحميل الملفات و لكنه لقد تم إسائة تهيئة شيء ما.  لم ينجح تحميل ملفك. لقد تم إرسال بريد إلكتروني إلى مديرك لإشعاره لكي يتمكن من إصلاح هذا الخطأ.  رمبا يمكنك محاولة تحميل هذا الملف لاحقاً.';
$string['clamemailsubject'] = '%s :: إشعار مضاد الفيروس clam';
$string['clamlost'] = 'إن مضاد الفيروس clam مهيأ للعمل عند تحميل ملف، و لكن المسار المعطى لمضاد الفيروس clam، %s، غير صحيح.';
$string['clamfailed'] = 'لقد فشل مضاد الفيروس clam بالتشغيل.  كانت رسالة الخطأ العائدة هي %s. و ها هي المخرجات من clam:';
$string['clamunknownerror'] = 'يوجد خطأ غير معروف في clam.';
$string['image'] = 'الصورة';
$string['filenotimage'] = 'الملف الذي قمت بتحميله ليس صورة صحيحة. يجب أن يكون ملفاً بصيغة PNG، JPEG أو GIF.';
$string['uploadedfiletoobig'] = 'الملف كبير جداً. يرجى مراجعة مديرك للمزيد من المعلومات.';
$string['notphpuploadedfile'] = 'لقد تم فقدان الملف أثناء عملية التحميل. ينبغي أن لا يحدث ذلك، يرجى مراجعة مديرك للمزيد من المعلومات.';
$string['virusfounduser'] = 'لقد تم فحص الملف الذي قمت بتحميله، %s ، عن طريق فاحص ضد الفيروس و تم إيجاده متأثراً بالفيروس! لم يتم تحميل ملفك بنجاح.';
$string['fileunknowntype'] = 'تعذر معرفة نوع ملفك المُحمَّل. يمكن أن يكون ملفك تالفاً، أو من المحتمل أن تكون مشكلة تهيئة. يرجى الاتصال بمديرك.';
$string['virusrepeatsubject'] = 'Warning: %s is a repeat virus uploader.';
$string['virusrepeatmessage'] = 'لقد قام المستخدم %s بتحميل ملفات متعددة قد تم فحصها عن طريق فاحص فيروس و وُجد أنها متاثرة بالفيروس.';

$string['phpuploaderror'] = 'حدث خطأ أثناء تحميل الملف: %s (رمز الخطأ %s)';
$string['phpuploaderror_1'] = 'يفوق الملف الذي تم تحميله الحد الأعلى من حجم الملف للتحميل التوجيهي في php.ini.';
$string['phpuploaderror_2'] = 'يفوق الملف الذي تم تحميله الحد الأعلى من حجم الملف التوجيهي الذي تم تحديده في نموذج HTML.';
$string['phpuploaderror_3'] = 'تم تحميل جزء فقط من الملف الذي تم تحميله.';
$string['phpuploaderror_4'] = 'لم يتم تحميل ملف.';
$string['phpuploaderror_6'] = 'مجلد مؤقت مفقود.';
$string['phpuploaderror_7'] = 'تعذر نسخ الملف على القرص.';
$string['phpuploaderror_8'] = 'File upload stopped by extension.';
$string['adminphpuploaderror'] = 'خطأ في تحميل الملف من المحتمل أن يكون سببه تهيئة الخادم.';

$string['youraccounthasbeensuspended'] = 'لقد تم إيقاف حسابك';
$string['youraccounthasbeensuspendedtext2'] = 'لقد تم إيقاف حسابك على %s من قبل %s.'; // @todo: more info?
$string['youraccounthasbeensuspendedreasontext'] = "لقد تم إيقاف حسابك على %s من قبل %s. السبب:\n\n%s";
$string['youraccounthasbeenunsuspended'] = 'لقد تم تفعيل حسابك';
$string['youraccounthasbeenunsuspendedtext2'] = 'لقد تم تفعيل حسابك على %s. يمكنك أن تدخل مرة أخرى و تستخدم الموقع.'; // can't provide a login link because we don't know how they log in - it might be by xmlrpc

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'bytes';

// countries

$string['country.af'] = 'أفغانستان';
$string['country.ax'] = 'جزر أولاند';
$string['country.al'] = 'ألبانيا';
$string['country.dz'] = 'الجزائر';
$string['country.as'] = 'ساموا الأمريكية';
$string['country.ad'] = 'أندورا';
$string['country.ao'] = 'أنغولا';
$string['country.ai'] = 'أنجويلا';
$string['country.aq'] = 'القارة القطبية الجنوبية';
$string['country.ag'] = 'أنتيغوا وبربودا';
$string['country.ar'] = 'الأرجنتين';
$string['country.am'] = 'أرمينيا';
$string['country.aw'] = 'أروبا';
$string['country.au'] = 'أستراليا';
$string['country.at'] = 'النمسا';
$string['country.az'] = 'أذربيجان';
$string['country.bs'] = 'جزر البهاما';
$string['country.bh'] = 'البحرين';
$string['country.bd'] = 'بنجلاديش';
$string['country.bb'] = 'بربادوس';
$string['country.by'] = 'روسيا البيضاء';
$string['country.be'] = 'بلجيكا';
$string['country.bz'] = 'بليز';
$string['country.bj'] = 'جمهورية بنين';
$string['country.bm'] = 'جزر برمودا';
$string['country.bt'] = 'مملكة بوتان';
$string['country.bo'] = 'بوليفيا';
$string['country.ba'] = 'جمهورية البوسنة والهرسك';
$string['country.bw'] = 'بوتسوانا';
$string['country.bv'] = 'جزيرة بوفيت';
$string['country.br'] = 'البرازيل';
$string['country.io'] = 'إقليم المحيط الهندي البريطاني';
$string['country.bn'] = 'بروناي دار السلام';
$string['country.bg'] = 'بلغاريا';
$string['country.bf'] = 'بوركينافاسو';
$string['country.bi'] = 'بوروندي';
$string['country.kh'] = 'كابوديا';
$string['country.cm'] = 'الكاميرون';
$string['country.ca'] = 'كندا';
$string['country.cv'] = 'الرأس الأخضر أو كاب فيردي';
$string['country.ky'] = 'جزر كايمان';
$string['country.cf'] = 'جمهورية أفريقيا الوسطى';
$string['country.td'] = 'جمهورية تشاد';
$string['country.cl'] = 'تشيلي';
$string['country.cn'] = 'الصين';
$string['country.cx'] = 'جزيرة كريسمس';
$string['country.cc'] = 'جزر كوكس';
$string['country.co'] = 'كولومبيا';
$string['country.km'] = 'جزر القمر';
$string['country.cg'] = 'كونغو';
$string['country.cd'] = 'جمهورية الكونغو الديمقراطية';
$string['country.ck'] = 'جزر كوك';
$string['country.cr'] = 'كوستا ريكا';
$string['country.ci'] = 'ساحل العاج';
$string['country.hr'] = 'جمهورية كرواتيا';
$string['country.cu'] = 'كوبا';
$string['country.cy'] = 'قبرص';
$string['country.cz'] = 'جمهورية التشيك';
$string['country.dk'] = 'الدنمارك';
$string['country.dj'] = 'جيبوتي';
$string['country.dm'] = 'دومينيكا';
$string['country.do'] = 'جمهورية الدومنيكان';
$string['country.ec'] = 'الإكوادور';
$string['country.eg'] = 'مصر';
$string['country.sv'] = 'السلفادور';
$string['country.gq'] = 'غينيا الاستوائية';
$string['country.er'] = 'دولة إرتريا';
$string['country.ee'] = 'إستونيا';
$string['country.et'] = 'إثيوبيا';
$string['country.fk'] = 'جزر فوكلاند';
$string['country.fo'] = 'جزر فارو';
$string['country.fj'] = 'فيجي';
$string['country.fi'] = 'فنلاندا';
$string['country.fr'] = 'فرنسا';
$string['country.gf'] = 'غويانا الفرنسية';
$string['country.pf'] = 'بولينزيا الفرنسية';
$string['country.tf'] = 'أراضي فرنسية جنوبية';
$string['country.ga'] = 'الغابون';
$string['country.gm'] = 'غامبيا';
$string['country.ge'] = 'جورجيا';
$string['country.de'] = 'ألمانيا';
$string['country.gh'] = 'غانا';
$string['country.gi'] = 'جبل طارق';
$string['country.gr'] = 'اليونان';
$string['country.gl'] = 'جرينلاند';
$string['country.gd'] = 'جرينادا';
$string['country.gp'] = 'جزر جوادلوب';
$string['country.gu'] = 'جزيرة غوام';
$string['country.gt'] = 'غواتيمالا';
$string['country.gg'] = 'جزيرة جيرنزي';
$string['country.gn'] = 'غينيا';
$string['country.gw'] = 'جمهورية غينيا بيساو';
$string['country.gy'] = 'غويانا';
$string['country.ht'] = 'هايتي';
$string['country.hm'] = 'جزيرة هيرد و جزر مكدونالد';
$string['country.va'] = 'الكرسي الرسولي (دولة الفاتيكان)';
$string['country.hn'] = 'هندوراس';
$string['country.hk'] = 'هونغ كونغ';
$string['country.hu'] = 'هنغاريا';
$string['country.is'] = 'آيسلندا';
$string['country.in'] = 'الهند';
$string['country.id'] = 'إندونيسيا';
$string['country.ir'] = 'جمهورية إيران الإسلامية';
$string['country.iq'] = 'العراق';
$string['country.ie'] = 'جزيرة أيرلندا';
$string['country.im'] = 'جزيرة مان';
$string['country.il'] = 'إسرئيل';
$string['country.it'] = 'إيطاليا';
$string['country.jm'] = 'جمايكا';
$string['country.jp'] = 'اليابان';
$string['country.je'] = 'جزيرة جيرزي';
$string['country.jo'] = 'الأردن';
$string['country.kz'] = 'كازاخستان';
$string['country.ke'] = 'كينيا';
$string['country.ki'] = 'جمهورية كيريباس';
$string['country.kp'] = 'كوريا الشمالية';
$string['country.kr'] = 'الجمهورية الكورية';
$string['country.kw'] = 'الكويت';
$string['country.kg'] = 'قرغيزستان';
$string['country.la'] = 'جمهورية لاوس الديمقراطية الشعبية';
$string['country.lv'] = 'لاتفيا';
$string['country.lb'] = 'لبنان';
$string['country.ls'] = 'مملكة ليسوتو';
$string['country.lr'] = 'ليبيريا';
$string['country.ly'] = 'الجماهيرية العربية الليبية';
$string['country.li'] = 'إمارة ليختنشتاين';
$string['country.lt'] = 'ليتوانيا';
$string['country.lu'] = 'لوكسمبورغ';
$string['country.mo'] = 'ماكاو';
$string['country.mk'] = 'جمهورية مقدونيا اليوغسلافية السابقة';
$string['country.mg'] = 'مدغشقر';
$string['country.mw'] = 'ملاوي';
$string['country.my'] = 'ماليزيا';
$string['country.mv'] = 'جزر المالديف';
$string['country.ml'] = 'مالي';
$string['country.mt'] = 'جمهورية مالطا';
$string['country.mh'] = 'جزر مارشال';
$string['country.mq'] = 'مارتينيك';
$string['country.mr'] = 'موريتانيا';
$string['country.mu'] = 'موريشوس';
$string['country.yt'] = 'مايوت';
$string['country.mx'] = 'المكسيك';
$string['country.fm'] = 'ولايات مايكرونزيا المتحدة';
$string['country.md'] = 'مولدوفا';
$string['country.mc'] = 'موناكو';
$string['country.mn'] = 'منغوليا';
$string['country.ms'] = 'منتسرات';
$string['country.ma'] = 'المغرب';
$string['country.mz'] = 'الموزمبيق';
$string['country.mm'] = 'ميانمار';
$string['country.na'] = 'ناميبيا';
$string['country.nr'] = 'ناورو';
$string['country.np'] = 'نيبال';
$string['country.nl'] = 'هولندا';
$string['country.an'] = 'جزر الأنتيل الهولندية';
$string['country.nc'] = 'كاليدونيا الجديدة';
$string['country.nz'] = 'نيوزيلندا';
$string['country.ni'] = 'نيكاراجوا';
$string['country.ne'] = 'النيجر';
$string['country.ng'] = 'نيجيريا';
$string['country.nu'] = 'نييوي';
$string['country.nf'] = 'جزيرة نورفولك';
$string['country.mp'] = 'جزر ماريانا الشمالية';
$string['country.no'] = 'النرويج';
$string['country.om'] = 'عمان';
$string['country.pk'] = 'باكستان';
$string['country.pw'] = 'جمهورية بالاو';
$string['country.ps'] = 'الأراضي الفلسطينية المحتلة';
$string['country.pa'] = 'باناما';
$string['country.pg'] = 'بابوا غينيا الجديدة';
$string['country.py'] = 'باراغواي';
$string['country.pe'] = 'البيرو';
$string['country.ph'] = 'الفلبين';
$string['country.pn'] = 'جزر بيتكيرن';
$string['country.pl'] = 'بولندا';
$string['country.pt'] = 'البرتغال';
$string['country.pr'] = 'بويرتوريكو';
$string['country.qa'] = 'قطر';
$string['country.re'] = 'Reunion';
$string['country.ro'] = 'رومانيا';
$string['country.ru'] = 'الاتّحاد الروسيّ';
$string['country.rw'] = 'رواندا';
$string['country.sh'] = 'جزيرة سانت هيلينا';
$string['country.kn'] = 'اتحاد سانت كريستوفر ونيفيس';
$string['country.lc'] = 'سانت لوسيا';
$string['country.pm'] = 'سان بيار وميكلون';
$string['country.vc'] = 'سانت فنسينت والجرينادينز';
$string['country.ws'] = 'ساموا';
$string['country.sm'] = 'سان مارينو';
$string['country.st'] = 'ساو تومي وبرينسيبي';
$string['country.sa'] = 'المملكة العربية السعودية';
$string['country.sn'] = 'السينيغال';
$string['country.cs'] = 'صربيا والجبل الأسود';
$string['country.sc'] = 'سيشيل';
$string['country.sl'] = 'جمهورية سيراليون';
$string['country.sg'] = 'سنغافورة';
$string['country.sk'] = 'سلوفاكيا';
$string['country.si'] = 'سلوفينيا';
$string['country.sb'] = 'جزر سليمان';
$string['country.so'] = 'صوماليا';
$string['country.za'] = 'جنوب أفريقيا';
$string['country.gs'] = 'جورجيا الجنوبية وجزر ساندويتش الجنوبية';
$string['country.es'] = 'إسبانيا';
$string['country.lk'] = 'سريلانكا';
$string['country.sd'] = 'السودان';
$string['country.sr'] = 'جمهورية سورينام';
$string['country.sj'] = 'سوالبارد و یان ماین';
$string['country.sz'] = 'مملكة سوازيلاند';
$string['country.se'] = 'السويد';
$string['country.ch'] = 'سويسرا';
$string['country.sy'] = 'الجمهورية العربية السورية';
$string['country.tw'] = 'تايوان، المقاطعة الصينية';
$string['country.tj'] = 'طاجكستان';
$string['country.tz'] = 'جمهورية تانزانيا المتحدة';
$string['country.th'] = 'تايلاند';
$string['country.tl'] = 'تيمور الشرقية';
$string['country.tg'] = 'توغو';
$string['country.tk'] = 'توكلو';
$string['country.to'] = 'مملكة تونجا ';
$string['country.tt'] = 'جمهورية ترينيداد وتوباجو';
$string['country.tn'] = 'تونس';
$string['country.tr'] = 'تركيا';
$string['country.tm'] = 'تركمانستان';
$string['country.tc'] = 'جزر تركس وكايكوس';
$string['country.tv'] = 'توفالو';
$string['country.ug'] = 'اوغاندا';
$string['country.ua'] = 'اوكرانيا';
$string['country.ae'] = 'الإمارات العربية المتحدة';
$string['country.gb'] = 'المملكة المتحدة';
$string['country.us'] = 'الولايات المتحدة';
$string['country.um'] = 'الولايات المتحدة الجزر الصغيرة النائية';
$string['country.uy'] = 'أوروغواي';
$string['country.uz'] = 'أوزباكستان';
$string['country.vu'] = 'فانواتو';
$string['country.ve'] = 'فنزويلا';
$string['country.vn'] = 'فيتنام';
$string['country.vg'] = 'الجزر العذراء البريطانية';
$string['country.vi'] = 'الجزر العذراء الأمريكية';
$string['country.wf'] = 'والس وفوتونا';
$string['country.eh'] = 'الصحراء الغربية';
$string['country.ye'] = 'اليمن';
$string['country.zm'] = 'زامبيا';
$string['country.zw'] = 'زمبابوي';

$string['nocountryselected'] = 'لم يتم اختيار البلد';

// general stuff that doesn't really fit anywhere else
$string['system'] = 'النظام';
$string['done'] = 'انتهى';
$string['back'] = 'عودة';
$string['backto'] = 'العودة إلى %s';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['formatpostbbcode'] = 'يمكنك تنسيق مشاركتك باستخدام BBCode. %sتعلم المزيد%s';
$string['Created'] = 'تم إنشاؤه';
$string['Updated'] = 'تم تحديثه';
$string['Total'] = 'الكلي';
$string['Visits'] = 'الزيارات';

// import related strings (maybe separated later)
$string['importedfrom'] = 'تم جلبه من  %s';
$string['incomingfolderdesc'] = 'Files imported from other networked hosts';
$string['remotehost'] = 'Remote host %s';

$string['Copyof'] = 'نسخة من %s';

// Profile views
$string['loggedinusersonly'] = 'السماح للمستخدمين مسجلي الدخول فقط';
$string['allowpublicaccess'] = 'السماح بالوصول العام';
$string['thisistheprofilepagefor'] = 'هذه صفحة المعلومات الشخصية لـ %s';
$string['viewmyprofilepage']  = 'عرض الصفحة الشخصية';
$string['editmyprofilepage']  = 'تعديل الصفحة الشخصية';
$string['usersprofile'] = "%s's الصفحة الشخصية";
$string['profiledescription'] = 'صفحة إلكترونية صفحتك الشخصية هي ما يراه الآخرون عندما يقومون بالنقر على اسمك أو أيقونة صفحتك الشخصية';

// Dashboard views
$string['mydashboard'] = 'لوحتي';
$string['editdashboard'] = 'تعديل';
$string['usersdashboard'] = "%s لوحة";
$string['dashboarddescription'] = 'صفحة إلكترونية لوحتك هي ما تراه على الصفحة الرئيسية عند بداية دخولك. صلاحية الوصول لهذه الصفحة لك فقط';
$string['topicsimfollowing'] = "المواضيع التي اتبعها";
$string['recentactivity'] = 'النشاط الأخير';
$string['mymessages'] = 'رسائلي';

$string['pleasedonotreplytothismessage'] = "الرجاء عدم الرد على هذه الرسالة.";
$string['deleteduser'] = 'مستخدم محذوف';

$string['theme'] = 'المظهر';
$string['choosetheme'] = 'اختر المظهر...';

// Home page info block
$string['Hide'] = 'إخفاء';
$string['createcollect'] = 'أنشئ و اجمع';
$string['createcollectsubtitle'] = 'طوِّر ملفك الشخصي';
$string['updateyourprofile'] = 'قم بتحديث  <a href="%s">معلوماتك الشخصية</a>';
$string['uploadyourfiles'] = 'حمل  <a href="%s">ملفاتك</a>';
$string['createyourresume'] = 'أنشئ <a href="%s">سيرتك الوظيفية</a>';
$string['publishablog'] = 'انشر <a href="%s">سجل ويب</a>';
$string['Organise'] = 'نظم و رتب';
$string['organisesubtitle'] = 'اعرض ملفك الشخصي ';
$string['organisedescription'] = 'صنف ملفك الشخصي حسب<a href="%s">الصفحات الإلكترونية.</a>  انشئ واجهات عرض مختلفة لجماهير مختلفة - فأنت تختار العناصر المراد تضمينها.';
$string['sharenetwork'] = 'المشاركة و الشبكة';
$string['sharenetworksubtitle'] = 'قابل أصدقاء و انضم إلى مجموعات';
$string['findfriendslinked'] = 'العثور على <a href="%s">الأصدقاء</a>';
$string['joingroups'] = 'انضم إلى <a href="%s">مجموعات</a>';
$string['sharenetworkdescription'] = 'يمكنك أن تعدل من لديه قابلية الوصول لكل واجهة عرض، و أن تعدل الفترة الزمنية لذلك.';
$string['howtodisable'] = 'لقد قمت بإخفاء صندوق المعلومات.  يمكنك التحكم بإظهاره و إخفاءه في <a href="%s">الإعدادات</a>.';
?>
