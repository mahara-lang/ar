<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// my groups
$string['groupname'] = 'اسم المجموعة';
$string['creategroup'] = 'إنشاء مجموعة';
$string['groupmemberrequests'] = 'طلبات العضوية المعلقة';
$string['membershiprequests'] = 'طلبات العضوية';
$string['sendinvitation'] = 'إرسال دعوة';
$string['invitetogroupsubject'] = 'تمت دعوتك للانضمام لمجموعة';
$string['invitetogroupmessage'] = 'لقد قام %s بدعوتك للانضمام لمجموعة، \'%s\'.  انقر على الرابط أدناه للمزيد من المعلومات.';
$string['inviteuserfailed'] = 'تذعر دعوة المستخدم';
$string['userinvited'] = 'تم إرسال الدعوة';
$string['addedtogroupsubject'] = 'تمت إضافتك إلى مجموعة';
$string['addedtogroupmessage'] = 'لقد قام %s بدعوتك للانضمام لمجموعة، \'%s\'.  انقر على الرابط أدناه لمشاهدة المجموعة';
$string['adduserfailed'] = 'تعذر إضافة المستخدم';
$string['useradded'] = 'تمت إضافة المستخدم';
$string['editgroup'] = 'تعديل المجموعة';
$string['savegroup'] = 'حفظ المجموعة';
$string['groupsaved'] = 'تم حفظ المجموعة بنجاح';
$string['invalidgroup'] = 'المجموعة غير موجودة';
$string['canteditdontown'] = 'لا يمكنك تعديل هذه المجموعة لأنك لا تملكها';
$string['groupdescription'] = 'وصف المجموعة';
$string['membershiptype'] = 'نوع وصف المجموعة';
$string['membershiptype.controlled'] = 'عضوية مضبوطة';
$string['membershiptype.invite']     = 'Invite Only';
$string['membershiptype.request']    = 'طلب عضوية';
$string['membershiptype.open']       = 'فتح عضوية';
$string['membershiptype.abbrev.controlled'] = 'مضبوط';
$string['membershiptype.abbrev.invite']     = 'دعوة';
$string['membershiptype.abbrev.request']    = 'طلب';
$string['membershiptype.abbrev.open']       = 'فتح';
$string['pendingmembers']            = 'الأعضاء المعلقين';
$string['reason']                    = 'السبب';
$string['approve']                   = 'الموافقة';
$string['reject']                    = 'الرفض';
$string['groupalreadyexists'] = 'يوجد مجموعة تحمل هذا الاسم';
$string['Created'] = 'تم إنشاؤه';
$string['groupadmins'] = 'مدراء المجموعة';
$string['Admin'] = 'مدير';
$string['grouptype'] = 'نوع المجموعة';
$string['publiclyviewablegroup'] = 'هل هذه مجموعة قابلة للمشاهدة من الجميع؟';
$string['publiclyviewablegroupdescription'] = 'هل تريد السماح للأشخاص غير مسجلين الدخول بمشاهدة هذه المجموعة، بما في ذلك المنتديات؟';
$string['Type'] = 'النوع';
$string['publiclyvisible'] = 'يشاهده الجميع';
$string['Public'] = 'عام';
$string['usersautoadded'] = 'هل تريد أن تتم إضافة المستخدمين تلقائياً؟';
$string['usersautoaddeddescription'] = 'هل تريد وضع جميع المستخدمين الجدد تلقائياً في هذه المجموعة؟';
$string['groupcategory'] = 'فئة المجموعة';
$string['allcategories'] = 'جميع الفئات';
$string['groupoptionsset'] = 'لقد تحديث خيارات المجموعة.';
$string['nocategoryselected'] = 'لا يوجد فئة مختارة';
$string['categoryunassigned'] = 'الفئة غير مخصصة';
$string['hasrequestedmembership'] = 'طلب أن يصبح عضواً في هذه المجموعة';
$string['hasbeeninvitedtojoin'] = 'تمت دعوته للانضمام لهذه المجموعة';
$string['groupinvitesfrom'] = 'تمت دعوته للانضمام إلى:';
$string['requestedmembershipin'] = 'طلب عضوية في:';
$string['viewnotify'] = 'عرض الإشعارات';
$string['viewnotifydescription'] = 'إذا تم التأشير عليه، سيت إرسال إشعار لكل عضو في المجموعة لكما شارك عضواً بأحد صفحاته الإلكترونية مع المجموعة. إن تفعيل هذا الخيار في مجموعات كبيرة جداً يمكن أن يصدر الكثير من الإشعارات.';

$string['editgroupmembership'] = 'تعديل عضوية المجموعة';
$string['editmembershipforuser'] = 'تعديل العضوية لـ %s';
$string['changedgroupmembership'] = 'تم تحديث عضوية المجموعة بنجاح.';
$string['changedgroupmembershipsubject'] = 'لقد تم تغيير عضويات مجموعتك';
$string['addedtogroupsmessage'] = "لقد قام %s بإضافتك إلى المجموعة أو المجموعات:\n\n%s\n\n";
$string['removedfromgroupsmessage'] = "لقد قام %s بحذفك من المجموعة أو المجموعات:\n\n%s\n\n";
$string['cantremoveuserisadmin'] = "لا يمكن للمعلم حذف المدراء و المعلمين الأعضاء الآخرين.";
$string['cantremovemember'] = "لا يمكن للمعلم حذف أعضاء.";

// Used to refer to all the members of a group - NOT a "member" group role!
$string['member'] = 'عضو';
$string['members'] = 'أعضاء';
$string['Members'] = 'أعضاء';

$string['memberrequests'] = 'طلبات عضوية';
$string['declinerequest'] = 'رفض الطلب';
$string['submittedviews'] = 'الصفحات الإلكترونية المرسلة';
$string['releaseview'] = 'Release view';
$string['invite'] = 'دعوة';
$string['remove'] = 'محذوف';
$string['updatemembership'] = 'تحديث العضوية';
$string['memberchangefailed'] = 'تعذر تحديث بعض معلومات العضوية';
$string['memberchangesuccess'] = 'لقد تم تغيير حالة العضوية';
$string['viewreleasedsubject'] = 'لقد تم إصدار صفحتك الإلكترونية "%s" من %s عن طريق %s';
$string['viewreleasedmessage'] = 'لقد تم إصدار صفحتك الإلكترونية "%s" من %s عن طريق %s';
$string['viewreleasedsuccess'] = 'لقد تم إصدار الصفحة الإلكترونية بنجاح';
$string['groupmembershipchangesubject'] = 'عضوية المجموعة: %s';
$string['groupmembershipchangedmessagetutor'] = 'لقد تمت ترقيتك إلى معلم في هذه المجموعة';
$string['groupmembershipchangedmessagemember'] = 'لقد إنزال رتبتك من معلم في هذه المجموعة';
$string['groupmembershipchangedmessageremove'] = 'لقد تم حذفك من هذه المجموعة';
$string['groupmembershipchangedmessagedeclinerequest'] = 'لقد تم رفض طلبك للانضمام لهذه المجموعة';
$string['groupmembershipchangedmessageaddedtutor'] = 'لقد تمت إضافتك كمعلم في هذه المجموعة';
$string['groupmembershipchangedmessageaddedmember'] = 'لقد تمت إضافتك كعضو في هذه المجموعة';
$string['leavegroup'] = 'مغادرة هذه المجموعة';
$string['joingroup'] = 'الانضمام لهذه المجموعة';
$string['requestjoingroup'] = 'اطلب الانضمام لهذه المجموعة';
$string['grouphaveinvite'] = 'لقد تمت دعوتك للانضمام لهذه المجموعة';
$string['grouphaveinvitewithrole'] = 'لقد تمت دعوتك للانضمام لهذه المجموعة بالوظيفة';
$string['groupnotinvited'] = 'لقد تمت دعوتك للانضمام لهذه المجموعة';
$string['groupinviteaccepted'] = 'تم قبول الدعوة بنجاح! أنت الآن عضو في المجموعة';
$string['groupinvitedeclined'] = 'تم رفض الدعوة بنجاح!';
$string['acceptinvitegroup'] = 'قبول';
$string['declineinvitegroup'] = 'رفض';
$string['leftgroup'] = 'لقد قمت الآن بمغادرة هذه المجموعة';
$string['leftgroupfailed'] = 'تعذر مغادرة المجموعة';
$string['couldnotleavegroup'] = 'لا يمكنك مغادرة هذه المجموعة';
$string['joinedgroup'] = 'أنت الآن عضو في المجموعة';
$string['couldnotjoingroup'] = 'لا يمكنك الانضمام لهذه المجموعة';
$string['grouprequestsent'] = 'تم إرسال طلب عضوية المجموعة';
$string['couldnotrequestgroup'] = 'تعذر إرسال طلب عضوية المجموعة';
$string['cannotrequestjoingroup'] ='لا يمكنك طلب الانضمام هذه المجموعة';
$string['groupjointypeopen'] = 'العضوية لهذه المجموعة مفتوحة. لا تتردد في الانضمام';
$string['groupjointypecontrolled'] = 'العضوية لهذه المجموعة محدودة. لا يمكنك الانضمام لهذه المجموعة.';
$string['groupjointypeinvite'] = 'العضوية في هذه المجموعة هي بالدعوة فقط.';
$string['groupjointyperequest'] = 'العضوية في هذه المجموعة هي بالطلب فقط.';
$string['grouprequestsubject'] = 'طلب عضوية مجموعة جديد';
$string['grouprequestmessage'] = '%s يرغب بالانضمام لمجموعتك %s';
$string['grouprequestmessagereason'] = "%s يرغب بالانضمام لمجموعتك %s. سببهم بالرغبة لانضمام هو:\n\n%s";
$string['cantdeletegroup'] = 'لا يمكنك حذف هذه المجموعة';
$string['groupconfirmdelete'] = 'هل انت متاكد من أنك تريد حذف هذه المجموعة؟';
$string['deletegroup'] = 'تم حذغ المجموعة بنجاح';
$string['allmygroups'] = 'كل مجموعاتي';
$string['groupsimin']  = 'المجموعات التي أنتمي إليها';
$string['groupsiown']  = 'المجموعات التي أملكها';
$string['groupsiminvitedto'] = 'المجموعات التي تمت دعوتي إليها';
$string['groupsiwanttojoin'] = 'المجموعات التي أريد الانضمام إليها';
$string['requestedtojoin'] = 'لقد طلبت الانضمام لهذه المجموعة';
$string['groupnotfound'] = 'لم يتم العثور على المجموعة ذات الرقم التعريفي %s';
$string['groupconfirmleave'] = 'هل أنت متأكد من انك تريد مغادرة هذه المجموعة؟';
$string['groupconfirmleavehasviews'] = 'هل أنت متأكد من انك تريد مغادرة هذه المجموعة؟ تستخدم بعض صفحاتك الإلكترونية هذه المجموعة للتحكم بالوصول، و مغادرتك هذه المجموعة يمكن ان يعني أنه من الممكن ان لن يتمكن أعضاء المجموعة الوصول إلى الصفحات الإلكترونية';
$string['cantleavegroup'] = 'لا يمكنك مغادرة هذه المجموعة';
$string['usercantleavegroup'] = 'هذا المستخدم لا يمكنه مغادرة هذه المجموعة';
$string['usercannotchangetothisrole'] = 'لا يمكن للمستخدم التغيير إلى هذه الوظيفة';
$string['leavespecifiedgroup'] = 'مغادرة المجموعة \'%s\'';
$string['memberslist'] = 'الأعضاء ';
$string['nogroups'] = 'لا يوجد مجموعات';
$string['deletespecifiedgroup'] = 'حذف المجموعة \'%s\'';
$string['requestjoinspecifiedgroup'] = 'طلب الانضمام للمجموعة \'%s\'';
$string['youaregroupmember'] = 'أنت عضو في هذه المجموعة';
$string['youaregrouptutor'] = 'أنت معلم في هذه المجموعة';
$string['youaregroupadmin'] = 'أنت مدير في هذه المجموعة';
$string['youowngroup'] = 'أنت تملك هذه المجموعة';
$string['groupsnotin'] = 'المجموعات التي لا أشارك بها';
$string['allgroups'] = 'جميع المجموعات';
$string['allgroupmembers'] = 'جميع أعضاء المجموعات';
$string['trysearchingforgroups'] = 'حاول%s البحث عن مجموعات%s لتنضم إليها';
$string['nogroupsfound'] = 'لم يتم العثور على مجموعات.';
$string['group'] = 'مجموعة';
$string['Group'] = 'مجموعة';
$string['groups'] = 'مجموعات';
$string['notamember'] = 'أنت لست عضواً في هذه المجموعة';
$string['notmembermayjoin'] = 'يجب أن تنضم إلى هذه المجموعة \'%s\' لتشاهد هذه الصفحة.';
$string['declinerequestsuccess'] = 'لقد تم رفض طلب عضوية مجموعة بنجاح.';
$string['notpublic'] = 'هذه المجموعة ليست عامة.';

// Bulk add, invite
$string['addmembers'] = 'إضافة أعضاء';
$string['invitationssent'] = '%d تم إرسال الدعوات';
$string['newmembersadded'] = 'إضاف %d أعطاء جدد';
$string['potentialmembers'] = 'الأعضاء المحتملين';
$string['sendinvitations'] = 'إرسال دعوات';
$string['userstobeadded'] = 'المستخدمين المراد إضافتهم';
$string['userstobeinvited'] = 'المستخدمين المراد دعوتهم';

// friendslist
$string['reasonoptional'] = 'السبب (اختياري)';
$string['request'] = 'طلب';

$string['friendformaddsuccess'] = 'تمت إضافة %s إلى قائمة الأصدقاء خاصتك';
$string['friendformremovesuccess'] = 'تم حذف %s من قائمة الأصدقاء خاصتك';
$string['friendformrequestsuccess'] = 'إرسال طلب صداقة إلى %s';
$string['friendformacceptsuccess'] = 'طلب صداقة مقبول';
$string['friendformrejectsuccess'] = 'طلب صداقة مرفوض';

$string['addtofriendslist'] = 'إضافة إلى الأصدقاء';
$string['requestfriendship'] = 'طلب صداقة';

$string['addedtofriendslistsubject'] = 'لقد قام %s بإضافتك كصديق';
$string['addedtofriendslistmessage'] = 'لقد قام %s بإضافتك كصديق! هذا يعني أن %s موجود أيضاً على قائمة الأصدقاء خاصتك. '
    . ' أنقر على الرابط أدناه لرؤية صفحة ملفهم الشخصي';

$string['requestedfriendlistsubject'] = 'طلب صداقة جديد';
$string['requestedfriendlistmessage'] = 'لقد طلب %s أن تقوم بإضافتهم كصديق.  '
    .' يمكنك فعل هذا إما من هذا الرابط أدناه، أو من صفحة قائمة الأصدقاء خاصتك';

$string['requestedfriendlistmessagereason'] = 'لقد طلب %s بأن تقوم بإضافتهم كصديق.'
    . ' يمكنك فعل هذا إما من هذا الرابط أدناه، أو من صفحة قائمة الأصدقاء خاصتك.'
    . ' سبب هو:
    ';

$string['removefromfriendslist'] = 'حذف من الأصدقاء';
$string['removefromfriends'] = 'حذف %s من الأصدقاء';
$string['confirmremovefriend'] = 'هل أنت متأكد من أنك تريد حذف هذا المستخدم من قائمة الأصدقاء خاصتك؟';
$string['removedfromfriendslistsubject'] = 'تم حذفه من قائمة الصدقاء';
$string['removedfromfriendslistmessage'] = 'لقد قام %s بحذفك من قائمة الأصدقاء خاصتك.';
$string['removedfromfriendslistmessagereason'] = '%s قاموا بحذفك من قائمة الأصدقاء خاصتك.  و كان سببهم: ';
$string['cantremovefriend'] = 'لا يمكنك حذف هذا المستخدم من قائمة الأصدقاء خاصتك';

$string['friendshipalreadyrequested'] = 'لقد طلبت أن تتم إضافتك إلى قائمة أصدقاء %s';
$string['friendshipalreadyrequestedowner'] = 'لقد طلب %s أن تتم إضافته إلى قائمة الأصدقاء خاصتك';
$string['rejectfriendshipreason'] = 'سبب رفض الطلب';
$string['alreadyfriends'] = 'أنت حالياً صديق مع %s';

$string['friendrequestacceptedsubject'] = 'تم قبول طلب الصداقة';
$string['friendrequestacceptedmessage'] = 'لقد قام %s بقبول طلب الصداقة الخاص بك و قد تمت إضافتهم إلى قائمة الأصدقاء خاصتك'; 
$string['friendrequestrejectedsubject'] = 'تم رفض طلب الصداقة';
$string['friendrequestrejectedmessage'] = 'لقد رفض %s طلب الصداقة الخاص بك.';
$string['friendrequestrejectedmessagereason'] = 'لقد رفض %s طلب الصداقة الخاص بك.  و كان السبب: ';

$string['allfriends']     = 'جميع الأصدقاء';
$string['currentfriends'] = 'الأصدقاء الحاليين';
$string['pendingfriends'] = 'الأصدقاء المعلقين';
$string['backtofriendslist'] = 'العودة إلى قائمة الأصدقاء';
$string['findnewfriends'] = 'العثور على أصدقاء جدد';
$string['Views']          = 'الصفحات الإلكترونية';
$string['Files']          = 'الملفات';
$string['seeallviews']    = 'مشاهدة جميع صفحات %s الإلكترونية...';
$string['noviewstosee']   = 'لا يمكنك مشاهدة أحد :(';
$string['whymakemeyourfriend'] = 'لهذا السبب يجب أن تجعلني صديق لك:';
$string['approverequest'] = 'قبول الطلب!';
$string['denyrequest']    = 'رفض الطلب';
$string['pending']        = 'معلق';
$string['trysearchingforfriends'] = 'حاول %sالبحث عن أصدقاء جدد%s لتجعل شبكتك أكبر!';
$string['nobodyawaitsfriendapproval'] = 'لا أحد ينتظر موافقتك ليصبح صديقاً لك';
$string['sendfriendrequest'] = 'إرسال طلب الصداقة!';
$string['addtomyfriends'] = 'إضافة إلى أصدقائي!';
$string['friendshiprequested'] = 'تم طلب الصداقة!';
$string['existingfriend'] = 'صديق موجود';
$string['nosearchresultsfound'] = 'تعذر العثور على نتائج بحث :(';
$string['friend'] = 'صديق';
$string['friends'] = 'أصدقاء';
$string['user'] = 'مستخدم';
$string['users'] = 'مستخدمين';
$string['Friends'] = 'أصدقاء';

$string['friendlistfailure'] = 'تعذر تعديل قائمة الصدقاء خاصتك';
$string['userdoesntwantfriends'] = 'لا يرغب هذا المستخدم بأي أصدقاء جدد';
$string['cannotrequestfriendshipwithself'] = 'لا يمكنك طلب صداقة نفسك';
$string['cantrequestfriendship'] = 'لا يمكنك طلب الصداقة مع هذا المستخدم';

// Messaging between users
$string['messagebody'] = 'إرسال رسالة'; // wtf
$string['sendmessage'] = 'إرسال رسالة';
$string['messagesent'] = 'تم إرسال الرسالة!';
$string['messagenotsent'] = 'تعذر إرسال الرسالة';
$string['newusermessage'] = 'رسالة جديدة من %s';
$string['newusermessageemailbody'] = 'لقد قام بإرسال رسالة لك.  لمشاهدة هذه الرسالة قم بزيارة%s';
$string['sendmessageto'] = 'إرسال رسالة إلى %s';
$string['viewmessage'] = 'عرض الرسالة';
$string['Reply'] = 'رد';

$string['denyfriendrequest'] = 'رفض طلب الصداقة';
$string['sendfriendshiprequest'] = 'إرسال طلب صداقة لـ%s';
$string['cantdenyrequest'] = 'طلب الصداقة هذا غير صحيح';
$string['cantrequestfrienship'] = 'لا يمكنك طلب الصداقة من هذا المستخدم';
$string['cantmessageuser'] = 'لا يمكنك إرسال رسالة لهذا المستخدم';
$string['cantviewmessage'] = 'لا يمكنك مشاهدة هذه الرسالة';
$string['requestedfriendship'] = 'تم طلب الصداقة';
$string['notinanygroups'] = 'غير موجود في أي من المجموعات';
$string['addusertogroup'] = 'إضافة إلى ';
$string['inviteusertojoingroup'] = 'دعوة إلى ';
$string['invitemembertogroup'] = 'قم بدعوة %s للانضمام إلى \'%s\'';
$string['cannotinvitetogroup'] = 'لا يمكنك دعوة هذا المستخدم إلى هذه المجموعة';
$string['useralreadyinvitedtogroup'] = 'لقد تمت دعوة هذا المستخدم مسبقاً إلى هذه المجموعة، أو أنه عضو حالي في المجموعة.';
$string['removefriend'] = 'حذف صديق';
$string['denyfriendrequestlower'] = 'رفض طلب الصداقة';

// Group interactions (activities)
$string['groupinteractions'] = 'نشاطات المجموعة';
$string['nointeractions'] = 'لا يوجد نشاطات في هذه المجموعة';
$string['notallowedtoeditinteractions'] = 'لا يسمح لك بإضافة او تعديل نشاطات في هذه المجموعة';
$string['notallowedtodeleteinteractions'] = 'لا يسمح لك بحذف نشاطات من هذه المجموعة';
$string['interactionsaved'] = '%s تم حفظه بنجاح';
$string['deleteinteraction'] = 'حذف %s \'%s\'';
$string['deleteinteractionsure'] = 'هل انت متأكد من أنك تريد فعل ذلك؟ لأنه لا يمكن الرجوع عنه.';
$string['interactiondeleted'] = '%s تم حذفه بنجاح';
$string['addnewinteraction'] = 'إضافة %s جديد';
$string['title'] = 'اللقب';
$string['Role'] = 'الوظيفة';
$string['changerole'] = 'تغيير الوظيفة';
$string['changeroleofuseringroup'] = 'تغيير وظيفة %s في %s';
$string['currentrole'] = 'الوظيفة الحالية';
$string['changerolefromto'] = 'تغيير الوظيفة من %s من إلى';
$string['rolechanged'] = 'تم تغيير الوظيفة';
$string['removefromgroup'] = 'حذف من المجموعة';
$string['userremoved'] = 'تم حذف المستخدم';
$string['About'] = 'معلومات عن';
$string['aboutgroup'] = 'معلومات عن %s';

$string['Joined'] = 'انضم';

$string['membersdescription:invite'] = 'الانضمام لهذه المجموعة بالدعوة فقط. يمكنك دعوة المستخدمين من خلال صفحات ملفاتهم الشخصية أو <a href="%s">إرسال دعوات متعددة مرة واحدة</a>.';
$string['membersdescription:controlled'] = 'هذه مجموعة ذات عضوية محدودة. يمكنك إضافة مستخدمين من خلال صفحات ملفاتهم الشخصية أو  <a href="%s">إضافة العديد من المستخدمين مرة واحدة</a>.';

// View submission
$string['submit'] = 'إرسال';
$string['allowssubmissions'] = 'Allows submissions';
?>
