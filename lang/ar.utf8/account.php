<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['changepassworddesc'] = 'كلمة المرور الجديدة';
$string['changepasswordotherinterface'] = 'يمكنك <a href="%s">تغيير كلمة المرور خاصتك</a> من خلال واجهة مختلفة';
$string['oldpasswordincorrect'] = 'هذه ليست كلمة مرورك الحالية';

$string['changeusernameheading'] = 'تغيير اسم المستخدم';
$string['changeusername'] = 'اسم المسخدم الجديد';
$string['changeusernamedesc'] = 'اسم المستخدم الذي تستخدمه للدخول إلى %s.  يجب أن تتكون كلمة المررو من 3-30 رمز، و يمكن ان تحتوي على حروف،أرقام و الرموز الأكثر شيوعاً باستثناء الفراغات.';

$string['accountoptionsdesc'] = 'خيارات الحساب العامة';
$string['friendsnobody'] = 'لا يمكن لأحد إضافتي كصديق';
$string['friendsauth'] = 'يطلب الأصدقاء الجدد تفويضاً مني';
$string['friendsauto'] = 'الأصدقاء الجدد مفوضين تلقائياً';
$string['friendsdescr'] = 'ضبط الأصدقاء';
$string['updatedfriendcontrolsetting'] = 'ضبط الأصدقاء الذين تم تحديثهم';

$string['wysiwygdescr'] = 'مُحرِّر HTML';
$string['on'] = 'فعَّال';
$string['off'] = 'إيقاف';
$string['disabled'] = 'مُعطَّل';
$string['enabled'] = 'فعَّال';

$string['messagesdescr'] = 'رسائل من مستخدمين آخرين';
$string['messagesnobody'] = 'لا تسمح لأي أحد بأن يرسل لي رسائل';
$string['messagesfriends'] = 'السماح للأشخاص على قائمة الأصدقاء خاصتي بإرسال رسائل لي';
$string['messagesallow'] = 'السماح لأي أحد بأن يرسل لي رسائل';

$string['language'] = 'اللغة';

$string['showviewcolumns'] = 'أظهر أدوات التحكم لإضافة أو حذف أعمدة عند تعديل صفحة إلكترونية';

$string['tagssideblockmaxtags'] = 'الحد الأعلى من الأوسمة في الصندوق';
$string['tagssideblockmaxtagsdescription'] = 'الحد الأعلى من الأوسمة المراد عرضها في صندوق السوم الخاص بك ';

$string['enablemultipleblogs'] = 'تفعيل المدونات المتعددة';
$string['enablemultipleblogsdescription']  = 'افتراضياً، لديك مدونة واحدة. فإذا كنت ترغب بحفظ أكثر من مدونة واحدة، قم بالتأشير على هذا الخيار.';
$string['disablemultipleblogserror'] = 'لا يمكنك تعطيل المدونات المتعددة مالم يكن لديك مدونة واحدة فقط';

$string['hiderealname'] = 'إخفاء الاسم الحقيقي';
$string['hiderealnamedescription'] = 'ضع إشارة على هذا المربع إذا قمت بضبط اسم عرض و لا تريد أن يكون المستخدمين الآخرين قادرين على العثور عليك عن طريق اسمك الحقيقي في البحث عن مستخدم';

$string['showhomeinfo'] = 'عرض معلومات عن مهارة على الصفحة الرئيسية';

$string['prefssaved']  = 'تم حفظ التفضيلات';
$string['prefsnotsaved'] = 'تعذر حفظ تفضيلاتك';

$string['maildisabled'] = 'تم تعطيل البريد الإلكتروني';
$string['maildisabledbounce'] =<<< EOF
لقد تم عملية إرسال البريد الإلكتروني إلى عنوان بريدك الإلكتروني لأنه قد تم إرجاع الكثير من الرسائل إلى الخادم.
الرجاء التحقق من أن بريدك الإلكتروني يعمل قبل أن تقوم بتفعيل البريد الإلكتروني في تفضيلات الحساب على %s.
EOF;
$string['maildisableddescription'] = 'لقد تم تعطيل عملية إرسال البريد الإلكتروني إلى حسابك. يمكنك <a href="%s">إعادة تغعيل بريدك الإلكتروني</a> من صفحة تفضيلات الحساب.';

$string['deleteaccount']  = 'حذف الحساب';
$string['deleteaccountdescription']  = 'إذا قمت بحذف حسابك، لن تعد معلومات صفحتك الشخصية و صفحاتك الإلكترونية مرئية للمستخدمين الآخرين.  سيكون محتوى أي مشاركات بالمنتديات قد قمت بكتابتها لا يزال مرئياً، و لكن لن يتم عرض اسم المؤلف.';
$string['accountdeleted']  = 'لقد تم حذف حسابك.';
?>
