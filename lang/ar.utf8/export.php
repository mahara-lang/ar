<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['allmydata'] = 'جميع بياناتي';
$string['chooseanexportformat'] = 'اختر صيغة للتصدير';
$string['clicktopreview'] = 'انقر للمشاهدة';
$string['collectionstoexport'] = 'Collections to export';
$string['creatingzipfile'] = 'إنشاء ملف مضغوط';
$string['Done'] = 'اكتمل';
$string['Export']     = 'تصدير';
$string['exportgeneratedsuccessfully'] = 'تم إرسال الصادر بنجاح. %sانقر لتنزيله%s';
$string['exportgeneratedsuccessfullyjs'] = 'تم إرسال الصادر بنجاح. %sمتابعة%s';
$string['exportingartefactplugindata'] = 'Exporting artefact plugin data';
$string['exportingartefacts'] = 'تصدير الأدوات';
$string['exportingartefactsprogress'] = 'تصدير الأدوات: %s/%s';
$string['exportingfooter'] = 'تصدير ذيل الصفحة';
$string['exportingviews'] = 'تصدير الصفحات الإلكترونية';
$string['exportingviewsprogress'] = 'تصدير الصفحات الإلكترونية: %s/%s';
$string['exportpagedescription'] = 'هذه الأداة تصدر جميع معلومات ملفك الشخصي و الصفحات الإلكنرونية، و لكنها لا تصدر إعدادات موقعك.';
$string['exportyourportfolio'] = 'قم بتصدير ملفك الشخصي';
$string['generateexport'] = 'إرسال التصدير';
$string['noexportpluginsenabled'] = 'لم يقم المدير بتفعيل برامج مساعدة للتصدير، لذا لا يمكنك استخدام هذه الميزة';
$string['justsomecollections'] = 'Just some of my Collections';
$string['justsomeviews'] = 'بعض صفحاتي الإلكترونية فقط';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'الرجاء الإنتظار بينما يتم إرسال تصديرك...';
$string['reverseselection'] = 'انتقاء عكسي';
$string['selectall'] = 'اختيار الكل';
$string['setupcomplete'] = 'اكتمل الإعداد';
$string['Starting'] = 'جاري التشغيل';
$string['unabletoexportportfoliousingoptions'] = 'تعذر تصدير ملفاً شخصياً باستخدام الخيارات المختارة';
$string['unabletogenerateexport'] = 'تعذر إرسال التصدير';
$string['viewstoexport'] = 'الصفحات الإلكترونية المراد تصديرها';
$string['whatdoyouwanttoexport'] = 'ماذا تريد أن تصدر؟';
$string['writingfiles'] = 'نسخ الملفات';
$string['youarehere'] = 'أنت هنا';
$string['youmustselectatleastonecollectiontoexport'] = 'You must select at least one collection to export';
$string['youmustselectatleastoneviewtoexport'] = 'يجب أن تختار صفحة إلكترونية واحدة على الأقل لتصديرها';
$string['zipnotinstalled'] = 'لا يوجد لدى نظامك أمر الضغط. الرجاء تثبيت برنامج الضغط من أجل تفعيل هذه الميزة';

?>
