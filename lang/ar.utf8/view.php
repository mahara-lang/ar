<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['createview']             = 'إنشاء صفحة إلكترونية';
$string['edittitle']              = 'طھط¹ط¯ظٹظ„ ط§ظ„ط¹ظ†ظˆط§ظ†';
$string['edittitleanddescription'] = 'طھط¹ط¯ظٹظ„ ط§ظ„ط¹ظ†ظˆط§ظ† ظˆ ط§ظ„ظˆطµظپ';
$string['editcontent']            = 'طھط¹ط¯ظٹظ„ ط§ظ„ظ…ط­طھظˆظ‰';
$string['editcontentandlayout']   = 'طھط¹ط¯ظٹظ„ ط§ظ„ظ…ط­طھظˆظ‰ ظˆ ط§ظ„ط´ظƒظ„ ط§ظ„ط®ط§ط±ط¬ظٹ';
$string['editaccess']             = 'طھط¹ط¯ظٹظ„ ط§ظ„ظˆطµظˆظ„';
$string['next']                   = 'ط§ظ„طھط§ظ„ظٹ';
$string['back']                   = 'ط¹ظˆط¯ط©';
$string['title']                  = 'ط¹ظ†ظˆط§ظ† ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط©';
$string['description']            = 'ظˆطµظپ ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط©';
$string['startdate']              = 'طھط§ط±ظٹط®/ظˆظ‚طھ ط¨ط¯ط، ط§ظ„ظˆطµظˆظ„';
$string['stopdate']               = 'طھط§ط±ظٹط®/ظˆظ‚طھ ط§ظ†طھظ‡ط§ط، ط§ظ„ظˆطµظˆظ„';
$string['stopdatecannotbeinpast'] = 'The stop date cannot be in the past';
$string['startdatemustbebeforestopdate'] = 'The start date must be before the stop date';
$string['unrecogniseddateformat'] = 'ظ†ظˆط¹ ط¨ظٹط§ظ†ط§طھ ط؛ظٹط± ظ…ط¹ط±ظپ';
$string['allowcommentsonview']    = 'إذا تم اختياره، سيتم السماح للمستخدمين بترك تعليقات.';
$string['ownerformat']            = 'صيغة عرض الاسم';
$string['ownerformatdescription'] = 'كيف تريد ان يرى اسمك الأشخاص الذين ينظرون إلى صفحتم الإلكترونية؟';
$string['profileviewtitle']       = 'ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ظ„ظ„طµظپط­ط© ط§ظ„ط´ط®طµظٹط©';
$string['dashboardviewtitle']  = 'ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ظ„ظ„ظˆط­ط©';
$string['editprofileview']        = 'طھط¹ط¯ظٹظ„ ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ظ„ظ„طµظپط­ط© ط§ظ„ط´ط®طµظٹط©';
$string['grouphomepageviewtitle'] = 'طµظپط­ط© ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ط§ظ„طµظپط­ط© ط§ظ„ط±ط¦ظٹط³ظٹط© ظ„ظ„ظ…ط¬ظ…ظˆط¹ط©';

// my views
$string['artefacts'] = 'الأدوات';
$string['myviews'] = 'صفحاتي الإلكترونية';
$string['groupviews'] = 'الصفحات الإلكترونية للمجموعة';
$string['institutionviews'] = 'الصفحات الإلكترونية للمؤسسة';
$string['reallyaddaccesstoemptyview'] = 'لا تحتوي صفحتك الإلكترونية على صناديق. هل ترغب فعلاً بمنح هؤلاء المستخدمين صلاحية الوصول إلى الصفحة الإلكترونية؟';
$string['viewdeleted'] = 'تم حذف الصفحة الإلكترونية';
$string['viewsubmitted'] = 'تم إرسال الصفحة الإلكترونية';
$string['deletethisview'] = 'احذف هذه الصفحة الإلكترونية';
$string['submitthisviewto'] = 'إرسال هذه الصفحة الإلكترونية إلى';
$string['forassessment'] = 'للتقييم';
$string['accessfromdate2'] = 'لا يمكن لأحد آخر أن يرى هذه الصفحة الإلكترونية قبل %s';
$string['accessuntildate2'] = 'لا يمكن لأحد آخر أن يرى هذه الصفحة الإلكترونية بعد %s';
$string['accessbetweendates2'] = 'لا يمكن لأحد آخر أن يرى هذه الصفحة الإلكترونية قبل %s أو بعد %s';
$string['artefactsinthisview'] = 'الأدوات في هذه الصفحة الإلكترونية';
$string['whocanseethisview'] = 'ظ…ظ† ظٹط³طھط·ظٹط¹ ظ…ط´ط§ظ‡ط¯ط© ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ظ‡ط°ظ‡';
$string['view'] = 'صفحة إلكترونية';
$string['views'] = 'صفحات إلكترونية';
$string['View'] = 'صفحة إلكترونية';
$string['Views'] = 'صفحات إلكترونية';
$string['viewsubmittedtogroup'] = 'لقد تم إرسال هذه الصفحة الإلكترونية إلى <a href="%s">%s</a>';
$string['viewsubmittedtogroupon'] = 'تم إرسال هذه الصفحة الإلكترونية إلى <a href="%s">%s</a> بتاريخ %s';
$string['nobodycanseethisview2'] = 'ط£ظ†طھ ظپظ‚ط· ظ…ظ† ظٹط³طھط·ظٹط¹ ظ…ط´ط§ظ‡ط¯ط© ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ظ‡ط°ظ‡';
$string['noviews'] = 'لا يوجد صفحات إلكترونية.';
$string['youhavenoviews'] = 'لا يوجد لديك صفحات إلكترونية.';
$string['youhaveoneview'] = 'يوجد لديك صفحة إلكترونية واحدة.';
$string['youhaveviews']   = 'يوجد لديك %s صفحات إلكترونية.';
$string['viewsownedbygroup'] = 'الصفحات الإلكترونية التي تملكها هذه المجموعة';
$string['viewssharedtogroup'] = 'الصفحات الإلكترونية المشتركة لهذه المجموعة';
$string['viewssharedtogroupbyothers'] = 'الصفحات الإلكترونية المشتركة لهذه المجموعة من قبل الاخرين';
$string['viewssubmittedtogroup'] = 'اصفحات الإلكترونية المرسلة إلى هذه المجموعة';
$string['submitaviewtogroup'] = 'إرسال صفحة إلكترونية إلى هذه المجموعة';
$string['youhavesubmitted'] = 'لقد قمت بإرسال <a href="%s">%s</a> إلى هذه المجموعة';
$string['youhavesubmittedon'] = 'أرسلت <a href="%s">%s</a> إلى هذه المجموعة بتاريخ %s';

// access levels
$string['public'] = 'ط¹ط§ظ…';
$string['loggedin'] = 'ط§ظ„ظ…ط³طھط®ط¯ظ…ظٹظ† ظ…ط³ط¬ظ„ظٹ ط§ظ„ط¯ط®ظˆظ„';
$string['friends'] = 'ط§ظ„ط£طµط¯ظ‚ط§ط،';
$string['groups'] = 'ط§ظ„ظ…ط¬ظ…ظˆط¹ط§طھ';
$string['users'] = 'ط§ظ„ظ…ط³طھط®ط¯ظ…ظٹظ†';
$string['friendslower'] = 'ط§ظ„ط£طµط¯ظ‚ط§ط،';
$string['grouplower'] = 'ظ…ط¬ظ…ظˆط¹ط©';
$string['tutors'] = 'ط§ظ„ظ…ط¹ظ„ظ…ظٹظ†';
$string['loggedinlower'] = 'ط§ظ„ظ…ط³طھط®ط¯ظ…ظٹظ† ظ…ط³ط¬ظ„ظٹ ط§ظ„ط¯ط®ظˆظ„';
$string['publiclower'] = 'ط¹ط§ظ…';
$string['everyoneingroup'] = 'ط§ظ„ظƒظ„ ظپظٹ ط§ظ„ظ…ط¬ظ…ظˆط¹ط©';
$string['token'] = 'URL سري';
$string['peoplewiththesecreturl'] = 'الأشخاص ذوي URL السري';

// view user
$string['inviteusertojoingroup'] = 'دعوة هذا المستخدم للانضمام لمجموعة';
$string['addusertogroup'] = 'إضافة هذا المستخدم إلى مجموعة';

// view view
$string['addedtowatchlist'] = 'لقد تمت إضافة هذه الصفحة الإلكترونية إلى لائحة المراقبة خاصتك';
$string['attachment'] = 'مرفق';
$string['removedfromwatchlist'] = 'لقد تم حذف هذه الصفحة الإلكترونية من لائحة المراقبة خاصتك';
$string['addtowatchlist'] = 'إضافة الصفحة الإلكترونية إلى قائمة المراقبة';
$string['removefromwatchlist'] = 'حذف الصفحة الإلكترونية من لائحة المراقبة';
$string['alreadyinwatchlist'] = 'هذه الصفحة الإلكترونية موجودة مسبقاً في لائحة المراقبة خاصتك';
$string['attachedfileaddedtofolder'] = "لقد تمت إضافة الملف المرفق %s إلى مجلد '%s' الخاص بك.";
$string['complaint'] = 'شكوى';
$string['date'] = 'التاريخ';
$string['notifysiteadministrator'] = 'إشعار مدير الموقع';
$string['print'] = 'طباعة';
$string['reportobjectionablematerial'] = 'Report objectionable material';
$string['reportsent'] = 'لقد تم إرسال تقريرك';
$string['viewobjectionableunmark'] = 'تم الإبلاغ عن هذه الصفحة الإلكترونية، أو شيء بداخلها، تحتوي على محتوى مرفوض. إذا لم تكن هذه هي الحالةن يمكنك النقر على هذا الزر لحذف هذا الإشعار و إبلاغ المدراء الآخرين.';
$string['notobjectionable'] = 'غير مرفوض';
$string['viewunobjectionablesubject'] = 'لقد قام %s بوضع إشارة على الصفحة الإلكترونية %s على أنها غير مرفوضة';
$string['viewunobjectionablebody'] = '%s has looked at %s by %s and marked it as no longer containing objectionable material.';
$string['updatewatchlistfailed'] = 'تعذر تحديث لائحة المراقبة';
$string['watchlistupdated'] = 'لقد تم تحديث لائحة المراقبة خاصتك';
$string['editmyview'] = 'تعديل صفحتي الإلكترونية';
$string['backtocreatemyview'] = 'العودة إلى إنشاء صفحتي الإلكترونية';
$string['viewvisitcount'] = 'زيارة (زيارات) الصفحة %d من %s إلى %s';

$string['friend'] = 'صديق';
$string['profileicon'] = 'أيقونة المعلومات الشخصية';

// general views stuff
$string['Added'] = 'تمت إضافته';
$string['allviews'] = 'جميع الصفحات الإلكترونية';

$string['submitviewconfirm'] = 'إذا قمت بإرسال \'%s\' إلى \'%s\' للتقييم، لن تتمكن من تعديل الصفحة الإلكترونية إلى أن ينهي معلمك وسم الصفحة الإلكترونية. هل أنت متاكد من أنك تريد إرسال هذه الصفحة الإلكترونية الآن؟';
$string['viewsubmitted'] = 'تم إرسال الصفحة الإلكترونية';
$string['submitviewtogroup'] = 'إرسال \'%s\' إلى \'%s\' للتقييم';
$string['cantsubmitviewtogroup'] = 'لا يمكنك إرسال هذه الصفحة الإلكترونية لهذه المجموعة للتقييم';

$string['cantdeleteview'] = 'لا يمكنك حذف هذه الصفحة الإلكترونية';
$string['deletespecifiedview'] = 'احذف الصفحة الإلكترونية "%s"';
$string['deleteviewconfirm'] = 'هل أنت متاكد فعلاً من أنك تريد حذف هذه الصفحة الإلكترونية؟ لأنه لايمكن الرجوع عن ذلك.';
$string['deleteviewconfirmnote'] = '<p><strong>ملاحظة:</strong> لن يتم حذف جميع صناديق المحتوى التي تمت إضافتها إلى الصفحة الإلكترونية. إلا أنه إذا تم إرسال أي تعليق ضد الصفحة الإلكترونية سيتم حذفه. ضع بعين الإعتبار عمل نسخه إحتياطيةعن الصفحة الإلكترونية أولاً.</p>';

$string['editaccessdescription'] = '<p>إفتراضياً، أنت فقط من يستطيع مشاهدة %s الحاص بك. يمكنك مشاركة %s الخاص بك مع الآخرين بإضافة قوانين الصول على هذه الصفحة.</p>
<p>حالما انتهيت، قم بالانتقال إلى الأسفل و انقر حفظ و متابعة.</p>';

$string['overridingstartstopdate'] = 'تجاهل تواريخ البدء/التوقف';
$string['overridingstartstopdatesdescription'] = 'إذا رغبت، يمكنك ضبط تاريخ البدء و/أو توقف تجاهلي. لن يتمكن الأشخاص الآخرين من مشهدة صفحتك الإلكترونية قبل تاريخ البدء و بعد تاريخ الانتهاء، بغض النظر عن أي صلاحية وصول أخرى قد منحك إياها.';

$string['emptylabel'] = 'انقر هنا لإدخال نص لهده الرقعة';
$string['empty_block'] = 'اختر أداة من الشجرة على اليسار لوضعها هنا';

$string['viewinformationsaved'] = 'لقد تم حفظ معلومات الصفحة الإلكترونية بنجاح';

$string['canteditdontown'] = 'لا يمكنك تعديل هذه الصفحة الإلكترونية لأنك لا تملكها';
$string['canteditsubmitted'] = 'لا يمكنك تعديل هذه الصفحة الإلكترونية لأنه قد تم إرسالها إلى "%s" للتقييم. ستضطر للانتظار إلى أن يقوم معلم بإصدار صفحتك الإلكترونية.';
$string['Submitted'] = 'تم إرساله';
$string['submittedforassessment'] = 'تم إرساله للتقييم';

$string['addtutors'] = 'إضافة معلمين';
$string['viewcreatedsuccessfully'] = 'تم إنشاء الصفحة الإلكترونية بنجاح';
$string['viewaccesseditedsuccessfully'] = 'تم حفظ الوصول للصفحة الإلكترونية بنجاح';
$string['viewsavedsuccessfully'] = 'تم حفظ الصفحة الإلكترونية بنجاح';

$string['invalidcolumn'] = 'العمود %s خارج عن النطاق';

$string['confirmcancelcreatingview'] = 'لم تكتمل هذه الصفحة الإلكترونية. هل ترغب فعلاً بالإلغاء؟';

// view control stuff

$string['editblockspagedescription'] = '<p>ظ‚ظ… ط¨ط³ط­ط¨ ظˆ ط¥ط¯ط±ط§ط¬ طµظ†ط§ط¯ظٹظ‚ ط§ظ„ظ…ط­طھظˆظ‰ ظ…ظ† ط§ظ„ط£ظ„ط³ظ†ط© ط£ط¯ظ†ط§ظ‡ ظ„ط¥ظ†ط´ط§ط، طµظپط­طھظƒ ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط©.</p>';
$string['displaymyview'] = 'ط¹ط±ط¶ طµظپط­طھظٹ ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط©';
$string['editthisview'] = 'طھط¹ط¯ظٹظ„ ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ظ‡ط°ظ‡';

$string['success.addblocktype'] = 'تم إضافة صندوق بنجاح';
$string['err.addblocktype'] = 'تعذر إضافة الصندوق إلى صفحتك الإلكترونية';
$string['success.moveblockinstance'] = 'تم نقل الصندوق بنجاح';
$string['err.moveblockinstance'] = 'تعذر نقل الصندوق إلى المكان المحدد';
$string['success.removeblockinstance'] = 'تم حذف الصندوق بنجاح';
$string['err.removeblockinstance'] = 'تعذر حذف الصندوق';
$string['success.addcolumn'] = 'تم إضافة عمود بنجاح';
$string['err.addcolumn'] = 'تعذر إضافة عمود جديد';
$string['success.removecolumn'] = 'تم حذف عمود بنجاح';
$string['err.removecolumn'] = 'تعذر حذف العمود';
$string['success.changetheme'] = 'تم تحديث المظهر بنجاح';
$string['err.changetheme'] = 'تعذر تحديث المظهر';

$string['confirmdeleteblockinstance'] = 'هل أنت متأكد من أنك ترغب بحذف هذا الصندوق؟';
$string['blockinstanceconfiguredsuccessfully'] = 'تم تهئية الصندوق بنجاح';
$string['blockconfigurationrenderingerror'] = 'فشل في التهيئة لأنه تعذر تصيير الصندوق.';

$string['blocksintructionnoajax'] = 'اختر صندوق و اختر أين ترغب بإضافته على صفحتك الإلكترونية. يمكنك اختيار مكان صندوق باستخدام إزرار الاسهم شريط العنوان الخاص بالصندوق';
$string['blocksinstructionajax'] = 'تعرض هذه المساحة كيف ستبدو صفحتك الإلكترونية.<br>اسحب الصناديق تحت هذا الخط لإضافتها إلى الشكل الخارجي لصفحتك الإلكترونية. يمكنك سحب صناديق داخل الشكل الخارجي لصفحتك الإلكترونية لتحديد أماكنها .';

$string['addnewblockhere'] = 'إضافة صندوق جديد هنا';
$string['add'] = 'إضافة';
$string['addcolumn'] = 'إضافة عمود';
$string['remove'] = 'حذف';
$string['removecolumn'] = 'احذف هذا العمود';
$string['moveblockleft'] = "تحريك الصندوق %s إلى اليسار";
$string['movethisblockleft'] = "انقل هذا الصندوق غلى اليسار";
$string['moveblockdown'] = "انقل الصندوق %s إلى الأسفل";
$string['movethisblockdown'] = "انقل هذا الصندوق إلى الأسفل";
$string['moveblockup'] = "انقل الصندوق %s إلى الأعلى";
$string['movethisblockup'] = "انقا هذا الصندوق إلى الأعلى";
$string['moveblockright'] = "انقل الصندوق %s إلى اليمين";
$string['movethisblockright'] = "انقل هذا الصندوق إلى اليمين";
$string['Configure'] = 'تهيئة';
$string['configureblock'] = 'تهيئة الصندوق %s';
$string['configurethisblock'] = 'هيأ هذا الصندوق';
$string['removeblock'] = 'احذف الصندوق %s';
$string['removethisblock'] = 'احذف هذا الصندوق';
$string['blocktitle'] = 'عنوان الصندوق';

$string['changemyviewlayout'] = 'طھط؛ظٹظٹط± ط§ظ„ط´ظƒظ„ ط§ظ„ط®ط§ط±ط¬ظٹ ظ„ظˆط§ط¬ظ‡ط© ط§ظ„ط¹ط±ط¶ ط®ط§طµطھظٹ';
$string['viewcolumnspagedescription'] = 'ط£ظˆظ„ط§ظ‹طŒ ظ‚ظ… ط¨ط§ط®طھظٹط§ط± ط¹ط¯ط¯ ط§ظ„ط£ط¹ظ…ط¯ط© ظپظٹ ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ط§ظ„ط®ط§طµط© ط¨ظƒ. ظˆ ظپظٹ ط§ظ„ط®ط·ظˆط© ط§ظ„طھط§ظ„ظٹط© ط³طھظƒظˆظ† ظ‚ط§ط¯ط±ط§ظ‹ ط¹ظ„ظ‰ طھط؛ظٹظٹط± ط¹ط±ط¶ ط§ظ„ط£ط¹ظ…ط¯ط©.';
$string['viewlayoutpagedescription'] = 'ظ‚ظ… ط¨ط§ط®طھظٹط§ط± ط§ظ„ط·ط±ظٹظ‚ط© ط§ظ„طھظٹ طھط±ط؛ط¨ ط£ظ† طھط¸ظ‡ط± ظپظٹظ‡ط§ ط§ظ„ط£ط¹ظ…ط¯ط© ظپظٹ ط§ظ„طµظپط­ط© ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط© ط®ط§طµطھظƒ.';
$string['changeviewlayout'] = 'طھط؛ظٹظٹط± ط§ظ„ط´ظƒظ„ ط§ظ„ط®ط§ط±ط¬ظٹ ظ„ط¹ظ…ظˆط¯ طµظپط­طھظٹ ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط©';
$string['backtoyourview'] = 'ط§ظ„ط¹ظˆط¯ط© ط¥ظ„ظ‰ طµظپط­طھظٹ ط§ظ„ط¥ظ„ظƒطھط±ظˆظ†ظٹط©';
$string['viewlayoutchanged'] = 'طھظ… طھط؛ظٹظٹط± ط§ظ„ط´ظƒظ„ ط§ظ„ط®ط§ط±ط¬ظٹ ظ„ظˆط§ط¬ظ‡ط© ط§ظ„ط¹ط±ط¶';
$string['numberofcolumns'] = 'ط¹ط¯ط¯ ط§ظ„ط£ط¹ظ…ط¯ط©';


$string['by'] = 'by';
$string['viewtitleby'] = '%s by <a href="%s">%s</a>';
$string['in'] = 'in';
$string['noblocks'] = 'عذراً، لا يوجد صناديق في هذه الفئة :(';
$string['Preview'] = 'معاينة';
$string['timeofsubmission'] = 'وقت الإرسال';

$string['50,50'] = $string['33,33,33'] = $string['25,25,25,25'] = 'عروض متساوية';
$string['67,33'] = 'Larger left column';
$string['33,67'] = 'Larger right column';
$string['25,50,25'] = 'Larger centre column';
$string['15,70,15'] = 'Much larger centre column';
$string['20,30,30,20'] = 'Larger centre columns';
$string['noviewlayouts'] = 'لا يوجد أشكال خارجية للصفحة الإلكترونية لصفحة عمود %s الإلكترونية';
$string['cantaddcolumn'] = 'لا يمكنك إضافة المزيد من الأعمدة إلى هذه الصفحة الإلكترونية';
$string['cantremovecolumn'] = 'لا يمكنك حذف آخر عمود من هذه الصفحة الإلكترونية';

$string['blocktypecategory.feeds'] = 'طھظ„ظ‚ظٹظ…ط§طھ ط®ط§ط±ط¬ظٹط©';
$string['blocktypecategory.fileimagevideo'] = 'ظ…ظ„ظپط§طھ ظˆ طµظˆط± ظˆ ظپظٹط¯ظٹظˆ';
$string['blocktypecategory.general'] = 'ط¹ط§ظ…';

$string['notitle'] = 'لا يوجد عنوان';
$string['clickformoreinformation'] = 'انقر هنا للمزيد من المعلومات و لترك تعليق';

$string['Browse'] = 'تصفح';
$string['Search'] = 'بحث';
$string['noartefactstochoosefrom'] = 'عذراً، لا يوجد أدوات للإختيار';

$string['access'] = 'الوصول';
$string['noaccesstoview'] = 'لا يوجد لديك تصريح للوصول إلى هذه الصفحة الإلكترونية';

$string['changeviewtheme'] = 'المظهر الذي اخترته لهذه الصفحة الإلكترونية لم يعد متوفراً لك. يرجى اختيار مظهراً مختلفاً.';

// Templates
$string['Template'] = 'قالب';
$string['allowcopying'] = 'السماح بالنسخ';
$string['templatedescription'] = 'ضع إشارة على هذا الصندوق إذا كنت ترغب بأن يكون الأشخاص القادرين على مشاهدة صفحتك الإلكترونية أن يتمكنوا من عمل نسخ لهم منها، بالإضافة للملفات و المجلدات التي تحويها.';
$string['templatedescriptionplural'] = 'ضع إشارة على هذا الصندوق إذا كنت ترغب بأن يكون الأشخاص القادرين على مشاهدة صفحتك الإلكترونية أن يتمكنوا من عمل نسخ لهم منها، بالإضافة للملفات و المجلدات التي تحويها.';
$string['choosetemplatepagedescription'] = '<p>هنا يمكنك البحث في الصفحات الإلكترونية المسموح بنسخها كنقطة بداية لعمل صفحة إلكترونية جديدة. و يمكنك مشاهدة معاينة لكل صفحة إلكترونية بالنقر على اسمها. و حالما تجد الصفحة الإلكترونية التي ترغب بنسخها، اضغط على زر "نسخ الصفحة الإلكترونية" الموافق لها لعمل نسخة و البدء بتعديلها.</p>';
$string['choosetemplategrouppagedescription'] = '<p>هنا يمكنك البحث في الصفحات الإلكترونية المسموح لهذه المجموعة بنسخها كنقطة بداية لعمل صفحة إلكترونية جديدة. و يمكنك مشاهدة معاينة لكل صفحة إلكترونية بالنقر على اسمها. و حالما تجد الصفحة الإلكترونية التي ترغب بنسخها، اضغط على زر "نسخ الصفحة الإلكترونية" الموافق لها لعمل نسخة و البدء بتعديلها.</p><p><strong>ملاحظة:</strong> لا يمكن حالياً للمجموعات عمل نسخ من المدونات أو التدوينات.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>هنا يمكنك البحث في الصفحات الإلكترونية المسموح لهذه المؤسسة بنسخها كنقطة بداية لعمل صفحة إلكترونية جديدة. و يمكنك مشاهدة معاينة لكل صفحة إلكترونية بالنقر على اسمها. و حالما تجد الصفحة الإلكترونية التي ترغب بنسخها، اضغط على زر "نسخ الصفحة الإلكترونية" الموافق لها لعمل نسخة و البدء بتعديلها.</p><p><strong>ملاحظة:</strong> لا يمكن حالياً للمؤسسات عمل نسخ من المدونات أو التدوينات.</p>';
$string['copiedblocksandartefactsfromtemplate'] = 'تم نسخ %d صناديق و %d أدوات من %s';
$string['filescopiedfromviewtemplate'] = 'الملفات المنسوخة من %s';
$string['viewfilesdirname'] = 'عرض الملفات';
$string['viewfilesdirdesc'] = 'ملفات من الصفحات الإلكترونية المنسوخة';
$string['thisviewmaybecopied'] = 'النسخ مسموح';
$string['copythisview'] = 'انسخ هذه الصفحة الإلكترونية';
$string['copyview'] = 'انسخ الصفحة الإلكترونية';
$string['createemptyview'] = 'إنشاء صفحة إلكترونية فارغة';
$string['copyaview'] = 'ظ†ط³ط® طµظپط­ط© ط¥ظ„ظƒطھط±ظˆظ†ظٹط©';
$string['Untitled'] = 'غير معنون';
$string['copyfornewusers'] = 'نسخة للمستخدمين الجدد';
$string['copyfornewusersdescription'] = 'كلما يتم إنشاء مستخدم جديد، قم بعمل نسخة شخصية من هذه الصفحة الإلكترونية تلقائياً في الملف الشخصي للمستخدم.';
$string['copyfornewmembers'] = 'نسخة لأعضاء المؤسسة الجدد';
$string['copyfornewmembersdescription'] = 'تلقائياً، قم بعمل نسخة من هذه الصفحة الإلكترونية لجميع أعضاء %s الجدد.';
$string['copyfornewgroups'] = 'نسخة للمجموعات الجديدة';
$string['copyfornewgroupsdescription'] = 'عمل نسخة من هذه الصفحة الإلكترونية في جميع المجموعات الجديدة ذات الأنواع التالية';
$string['searchviews'] = 'البحث عن صفحات إلكترونية';
$string['searchowners'] = 'البحث عن مالكين';
$string['owner'] = 'مالك';
$string['Owner'] = 'مالك';
$string['owners'] = 'مالكين';
$string['show'] = 'عرض';
$string['searchviewsbyowner'] = 'البحث عن صفحات إلكترونية حسب المالك:';
$string['selectaviewtocopy'] = 'اختر الصفحة الإلكترونية التي ترغب بنسخها:';
$string['listviews'] = 'عرض الصفحات الإلكترونية في لائحة';
$string['nocopyableviewsfound'] = 'لا يوجد صفحات إلكترونية يمكنك نسخها';
$string['noownersfound'] = 'لم يتم العثور على مالكين';
$string['viewsby'] = 'الصفحات الإلكترونية حسب %s';
$string['Preview'] = 'معاينة';
$string['viewscopiedfornewusersmustbecopyable'] = 'يجب أن تسمح بالنسخ قبل أن تتمكن من ضبط صفحة إلكترونية ليتم نسخها للمستخدمين الجدد.';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'يجب أن تسمح بالنسخ قبل أن تتمكن من ضبط صفحة إلكترونية ليتم نسخها للمجموعات الجديدة.';
$string['copynewusergroupneedsloggedinaccess'] = 'يجب أن تمنح الصفحات الإلكترونية المنسوخة للمستخدمين الجدد و المجموعات الجديدة صلاحية الوصول للمستخدمين مسجلي الدخول.';
$string['viewcopywouldexceedquota'] = 'نسخ هذه الصفحة الإلكترونية يمكن أن يفوق كوتا الملف المخصصة لك.';

$string['blockcopypermission'] = 'تصريح نسخ صندوق';
$string['blockcopypermissiondesc'] = 'يمكنك اختيار كيف سيتم نسخ هذا الصندوق، إذا قمت بالسماح للمستخدمين الآخرين بنسخ هذه الصفحة الإلكترونية';

// View types
$string['dashboard'] = 'اللوحة';
$string['profile'] = 'المعلومات الشخصية';
$string['portfolio'] = 'الملف الشخصي';
$string['grouphomepage'] = 'الصفحة الرئيسية للمجموعة';

$string['grouphomepagedescription'] = 'الصفة الإلكترونية للصفحة الرئيسية للمجموعة هي المحتوى الذي يظهر على About tab لهذه المجموعة';

?>
