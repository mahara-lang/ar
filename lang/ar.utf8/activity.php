<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage notification-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['typemaharamessage'] = 'رسالة من النظام';
$string['typeusermessage'] = 'رسالة من المستخدمين الآخرين';
$string['typewatchlist'] = 'قائمة المراقبة';
$string['typeviewaccess'] = 'الوصول إلى صفحة إلكترونية جديدة';
$string['typecontactus'] = 'اتصل بنا';
$string['typeobjectionable'] = 'محتوى مرفوض';
$string['typevirusrepeat'] = 'إعادة تحميل الفيروس';
$string['typevirusrelease'] = 'Virus flag release';
$string['typeadminmessages'] = 'رسائل من الإدارة';
$string['typeinstitutionmessage'] = 'رسالة من المؤسسة';
$string['typegroupmessage'] = 'رسالة من المجموعة';

$string['type'] = 'نوع النشاط';
$string['attime'] = 'at';
$string['prefsdescr'] = 'إذا قمت باختيار أحد خيارات البريد الإلكتروني، ستبقى الإشعارات تصل إلى صندوق الوارد الخاص بك، و لكنه سيتم التأشير عليها على أنها مقروءة.';

$string['subject'] = 'الموضوع';
$string['date'] = 'التاريخ';
$string['read'] = 'مقروء';
$string['unread'] = 'غير مقروء';

$string['markasread'] = 'التأشير عليها على أنها مقروءة';
$string['selectall'] = 'اختر الكل';
$string['recurseall'] = 'Recurse all';
$string['alltypes'] = 'جميع الأنواع';

$string['markedasread'] = 'قم بالتأشير على إشعاراتك على أنها مقروءة';
$string['failedtomarkasread'] = 'تعذر التأشير على إشعاراتك على انها مقروءة';

$string['deletednotifications'] = 'تم حذف إشعارات %s';
$string['failedtodeletenotifications'] = 'تعذر حذف إشعاراتك';

$string['stopmonitoring'] = 'إيقاف المراقبة';
$string['viewsandartefacts'] = 'الأدوات و الصفحات الإلكترونية';
$string['views'] = 'الصفحات الإلكترونية';
$string['artefacts'] = 'الأدوات';
$string['groups'] = 'المجموعات';
$string['monitored'] = 'مُراقَب';
$string['stopmonitoring'] = 'إيقاف المراقبة';

$string['stopmonitoringsuccess'] = 'تم إيقاف المراقبة بنجاح';
$string['stopmonitoringfailed'] = 'تعذر إيقاف المراقبة';

$string['newwatchlistmessage'] = 'نشاط جديد على قائمة المراقبة خاصتك';
$string['newwatchlistmessageview'] = '%s قام بتغيير صفحته الإلكترونية "%s"';

$string['newviewsubject'] = 'تم إنشاء صفحة إلكترونية جديدة';
$string['newviewmessage'] = 'لقد قام %s بإنشاء صفحة إلكترونية جديدة "%s"';

$string['newcontactusfrom'] = 'نموذج اتصل بنا جديد';
$string['newcontactus'] = 'اتصل بنا جديد';

$string['newviewaccessmessage'] = 'لقد قام %s بإضافتك إلى قائمة الوصول للصفحة الإلكترونية المسماه "%s"  ';
$string['newviewaccessmessagenoowner'] = 'لقد تمت إضافتك إلى قائمة الوصول للصفحة الإلكترونية المسماه "%s"';
$string['newviewaccesssubject'] = 'وصول جديد للصفحة الإلكترونية';

$string['viewmodified'] = 'قامو بتغيير صفحتهم الإلكترونية';
$string['ongroup'] = 'على المجموعة';
$string['ownedby'] = 'يملكها';

$string['objectionablecontentview'] = 'لقد قام %s بالإبلاغ عن محتوى مرفوض على الصفحة الإلكترونية  "%s"';
$string['objectionablecontentviewartefact'] = 'لقد قام %s بالإبلاغ عن محتوى مرفوض على الصفحة الإلكترونية  "%s" في "%s"';

$string['newgroupmembersubj'] = '%s هو الآن عضو في المجموعة!';
$string['removedgroupmembersubj'] = '%s لم يعد عضو في المجموعة';

$string['addtowatchlist'] = 'إضافة إلى قائمة المراقبة';
$string['removefromwatchlist'] = 'حذف من قائمة المراقبة';

$string['missingparam'] = 'Required parameter %s was empty for activity type %s';

$string['institutionrequestsubject'] = 'لقد طلب %s أن يصبح عضواً في %s.';
$string['institutionrequestmessage'] = 'يمكنك إضافة مستخدمين للمؤسسة على صفحة أعضاء المؤسسة:';

$string['institutioninvitesubject'] = 'لقد تمت دعوتك لللإنضمام إلى المؤسسة  %s.';
$string['institutioninvitemessage'] = 'يمكنك تأكيد عضويتك في هذه المؤسسة على صفحة إعدادات مؤسستك:';

$string['deleteallnotifications'] = 'حذف جميع الإشعارات';
$string['reallydeleteallnotifications'] = 'هل أنت متاكد من أنك تريد حذف جميع إشعاراتك؟';
 
$string['viewsubmittedsubject'] = 'تم إرسال الصفحة الإلكترونية إلى %s';
$string['viewsubmittedmessage'] = 'لقد قاموا بإرسال صفحتهم الإلكترونية "%s" إلى %s';

?>
