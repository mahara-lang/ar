<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-wall
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'الحائط';
$string['otherusertitle'] = "%s حائط ";
$string['description'] = 'عرض الحقل الذي يستطيع الأشخاص إضافة تعليقات عليه';
$string['noposts'] = 'لا يوجد مشاركات حائط للعرض';
$string['makeyourpostprivate'] = 'هل تريد عمل مشاركتك خاصة؟';
$string['viewwall'] = 'عرض الحائط';
$string['backtoprofile'] = 'العودة للصفحة الشخصية';
$string['wall'] = 'الحائط';
$string['wholewall'] = 'عرض الحائط كاملاً';
$string['reply'] = 'رد';
$string['delete'] = 'حذف المشاركة';
$string['deletepost'] = 'حذف المشاركة';
$string['Post'] = 'مشاركة';
$string['deletepostsure'] = 'هل أنت متأكد من أنك تريد القيام بهذا؟ لأنه لا يمكن التراجع عنه.';
$string['deletepostsuccess'] = 'تم حذف المشاركة بنجاح';
$string['maxcharacters'] = "%s أحرف في المشاركة كحد أقصى.";
$string['sorrymaxcharacters'] = "عذراً، لا يمكن أن تتعدى مشاركتك %s حرف.";

// Config strings
$string['postsizelimit'] = "الحجم المحدد للمنشور";
$string['postsizelimitdescription'] = "يمكنك تحديد حجم منشورات الحائط هنا. المنشورات الحالية لن تتغير";
$string['postsizelimitmaxcharacters'] = "الحد الأقصى من الحروف";
$string['postsizelimitinvalid'] = "هذا رقم غير صحيح.";
$string['postsizelimittoosmall'] = "لا يمكن أن يكون هذا الحد أقل من صفر.";

?>
