<?php
/**
 * Creative Commons License Block type for Mahara
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-creativecommons
 * @author     Francois Marier <francois@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009 Catalyst IT Ltd
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'رخصة المشاع الإبداعي';
$string['description'] = 'إرفاق رخصة مشاع إبداعي بصفحتك الإلكترونية';
$string['blockcontent'] = 'محتوى الصندوق';

$string['alttext'] = 'رخصة المشاع الإبداعي';
$string['licensestatement'] = "هذا العمل مرخص بموجب <a rel=\"license\" href=\"%s\">Creative Commons %s 3.0 Unported License</a>.";
$string['sealalttext'] = 'هذه الرخصة مقبولة للأعمال الثقافية المجانية.';

$string['config:noncommercial'] = 'هل ترغب بالاستخدامات التجارية لعملك؟';
$string['config:noderivatives'] = 'هل ترغب بالسماح بإجراء تعديلات على عملك؟';
$string['config:sharealike'] = 'نعم، لطالما أن الآخرين يشاركون بذلك بالمثل';

$string['by'] = 'Attribution';
$string['by-sa'] = 'Attribution-Share Alike';
$string['by-nd'] = 'Attribution-No Derivative Works';
$string['by-nc'] = 'Attribution-Noncommercial';
$string['by-nc-sa'] = 'Attribution-Noncommercial-Share Alike';
$string['by-nc-nd'] = 'Attribution-Noncommercial-No Derivative Works';

?>
