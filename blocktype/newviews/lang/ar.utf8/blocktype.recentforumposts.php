<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2009 Nigel McNie (http://nigel.mcnie.name/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-recentforumposts
 * @author     Nigel McNie
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009 Nigel McNie http://nigel.mcnie.name/
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'منشورات المنتدى الجديدة لـ <group>';
$string['description'] = 'عرض منشورات المنتدى الجديدة لمجموعة';

$string['group'] = 'مجموعة';
$string['nogroupstochoosefrom'] = 'عذراً، لا يوجد مجموعات للاختيار منها';
$string['poststoshow'] = 'الحد الأعلى من المنشورات المراد عرضها';
$string['poststoshowdescription'] = 'مابين 1 و 100';
$string['recentforumpostsforgroup'] = "منشورات المنتدى الجديدة لـ %s";

?>
