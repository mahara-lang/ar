<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'التلقيم الخارجي';
$string['description'] = 'تضمين تلقيم خارجي بصيغة RSS أو ATOM';
$string['feedlocation'] = 'موقع التلقيم';
$string['feedlocationdesc'] = 'وصلة أنترنت لتلقيم RSS أو ATOM صحيح';
$string['itemstoshow'] = 'عناصر للعرض';
$string['itemstoshowdescription'] = 'ما بين 1 و 20';
$string['showfeeditemsinfull'] = 'هل تريد عرض عناصر التلقيم بشكل كامل?';
$string['showfeeditemsinfulldesc'] = '‘إما أن تظهر تلخيصاً للتعليقات، أو أن ظهر النص كاملاً لكل تعليق';
$string['invalidurl'] = 'That URL is invalid. You can only view feeds for http and https URLs.';
$string['invalidfeed'] = 'يبدو أن التعليق غير صحيح. كان الخطأ المعلن عنه هو: %s';
$string['lastupdatedon'] = 'تم آخر تحديث بتاريخ %s';
$string['defaulttitledescription'] = 'إذا تركت هذا فارغاً، سيتم استخدام عنوان التعليق';
?>
