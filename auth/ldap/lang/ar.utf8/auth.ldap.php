<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'LDAP';
$string['description'] = 'التحقق من صحة خادم LDAP';
$string['notusable'] = 'يرجى تثبيت امتداد خادم PHP LDAP';

$string['contexts'] = 'Contexts';
$string['distinguishedname'] = 'اسم مميز';
$string['hosturl'] = 'URL مضيف';
$string['ldapfieldforemail'] = 'حقل LDAP للبريد الإلكتروني';
$string['ldapfieldforfirstname'] = 'حقل LDAP للاسم الأول';
$string['ldapfieldforsurname'] = 'حقل LDAP لاسم العائلة';
$string['ldapversion'] = 'إصدار LDAP';
$string['password'] = 'كلمة المرور';
$string['searchsubcontexts'] = 'Search subcontexts';
$string['userattribute'] = 'خاصية المستخدم';
$string['usertype'] = 'نوع المستخدم';
$string['weautocreateusers'] = 'نحن ننشىء المستخدمين آلياً';
$string['updateuserinfoonlogin'] = 'تحديث معلومات المستخدم عند تسجيل الدخول';
$string['cannotconnect'] = 'تعذر الاتصال بأي من مضيفي LDAP';
?>
