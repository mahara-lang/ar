<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Piers Harding <piers@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

//$string['defaultidpidentity'] = 'Default IdP Identity Service';
$string['defaultinstitution'] = 'المؤسسة الافتراضية';
$string['description'] = 'التأكد من صحة خدمة SAML 2.0 IdP';
$string['errorbadinstitution'] = 'Institution for connecting user not resolved';
$string['errorretryexceeded'] = 'لقد فاق الحد الأعلى من عدد المحاولات الـ (%s) - لا بد أنه يوجد مشكلة في خدمة المطابقة';
$string['errnosamluser'] = 'لم يتم العثور على مستخدم';
$string['errorbadlib'] = 'مجلد SimpleSAMLPHP lib %s غير صحيح.';
$string['errorbadconfig'] = 'SimpleSAMLPHP config directory %s is in correct.';
$string['errorbadcombo'] = 'يمكنك اختيار إنشاء المستخدم تلقائياً إذا لم تكن قد اخترت مستخدم بعيد';
//$string['idpidentity'] = 'IdP Identity Service';
$string['institutionattribute'] = 'خاصية المؤسسة (تحتوي "%s")';
$string['institutionvalue'] = 'Institution value to check against attribute';
$string['institutionregex'] = 'هل يتطابق المقطع الجزئي مع الاسم المختصر للمؤسسة';
$string['notusable'] = 'يرجى تثبيت مكتبات الـSimpleSAMLPHP SP';
$string['samlfieldforemail'] = 'حقل SSO للبريد الإلكتروني';
$string['samlfieldforfirstname'] = 'حقل SSO للاسم الول';
$string['samlfieldforsurname'] = 'حقل SSO لاسم العائلة';
$string['title'] = 'SAML';
$string['updateuserinfoonlogin'] = 'تحديث بيانات المستخدم عند تسجيل الدخول';
$string['userattribute'] = 'خاصية المستخدم';
$string['simplesamlphplib'] = 'مجلد مكتبة SimpleSAMLPHP';
$string['simplesamlphpconfig'] = 'مجلد تهئية SimpleSAMLPHP';
$string['weautocreateusers'] = 'نحن ننشىء المستخدمين تلقائياً';
$string['remoteuser'] = 'مطابقة خاصية اسم المستخدم مع اسم المستخدم البعيد';
?>
