<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['internal'] = 'داخلي';
$string['title'] = 'داخلي';
$string['description'] = 'التحقق من صحة قاعدة بيانات مهارة';

$string['completeregistration'] = 'إتمام عملية التسجيل';
$string['emailalreadytaken'] = 'لقد قام عنوان البريد الإلكتروني هذا بالتسجيل مسبقاً هنا';
$string['iagreetothetermsandconditions'] = 'أوافق على الشروط و الأحكام';
$string['passwordformdescription'] = 'يجب أن تتكون كلمة المرور من ستة حروف على الأقل و أن تحتوي على رقم واحد و حرفين على الأقل';
$string['passwordinvalidform'] = 'يجب أن تتكون كلمة المرور من ستة حروف على الأقل و أن تحتوي على رقم واحد و حرفين على الأقل';
$string['registeredemailsubject'] = 'لقد قمت بالتسجيل في %s';
$string['registeredemailmessagetext'] = 'مرحباً %s,

شكراً لتسجيلك حساباً على %s. يرجى اتباع هذا الرابط لاتمام عملية التسجيل:

%sregister.php?key=%s

سينتهي الرابط خلال 24 ساعة.

--
،أفضل التحيات
فريق %s';
$string['registeredemailmessagehtml'] = '<p>مرحباً %s,</p>
<p>شكراً لتسجيلك حساباً على %s. يرجى اتباع هذا الرابط لاتمام عملية التسجيل:</p>
<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>
<p>سينتهي الرابط خلال 24 ساعة.</p>

<pre>--
،أفضل التحيات
فريق %s</pre>';
$string['registeredok'] = '<p>لقد سجلت بنجاح. يرجى تفقد حساب بريدك الإلكتروني من أجل تعليمات حول كيفية تفعيل حسابك</p>';
$string['registrationnosuchkey'] = 'عذراً، لا يبدو أنه يوجد تسجيلاً بهذا المفتاح. ربما أنك انتظرت أكثر من 24 ساعة لإتمام تسجيلك؟ أو، أنه خطأنا.';
$string['registrationunsuccessful'] = 'عذراً، لم تكن عملية تسجيلك ناجحة. هذا خطأنا و ليس خطأك. يرجى المحاولة مرة أحرى لاحقاً.';
$string['usernamealreadytaken'] = 'عذراً، اسم المستخدم هذا مأخوذ مسبقاً';
$string['usernameinvalidform'] = 'يمكن أن تحتوي اسماء المستخدمين على حروف، أرقام و الرموز الأكثر شيوعاً، و يجب أن تتكون من 3 - 30 حرف. و لا يسمح بالفراغات.';
$string['youmaynotregisterwithouttandc'] = 'لا يمكنك التسجيل إذا لم توافق على الإلتزام <a href="terms.php">بالشروط و الأحكام</a>';
